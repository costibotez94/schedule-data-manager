<?php

/**
 * Setting Model
 */
class Schedule {
    public $table = array();

    /**
     * Schedule Constructor
     * @param Array $cell
     */
    public function __construct($table) {
        $this->table = $table;
    }

    /**
     * Get all settings from database
     * @return Array
     */
    public static function getAll() {
        $list = [];
        $db = DB::getInstance();
        $request = $db->query('SELECT * FROM settings');

        foreach ($request->fetchAll() as $setting) {
            $list[] = new Setting($setting['setting_key'], $setting['setting_value']);
        }

        return $list;
    }

    /**
     * Update setting from database
     * @param String $setting_key
     * @param String $setting_value
     * @return void
     */
    public static function update($setting_key, $setting_value) {
        $db = DB::getInstance();
        try {
            if($setting_key && $setting_value) {
                $request = $db->prepare('UPDATE settings SET setting_value = :setting_value WHERE setting_key = :setting_key');
                $request->execute(array(
                    'setting_key'  => $setting_key,
                    'setting_value' => $setting_value
                ));
            }
        } catch (Exception $e) {
            echo 'Caught exception:' . $e->getMessage();
        }
    }

    /**
     * Get global major ID
     * @return Int
     */
    public static function getSettedMajorID() {
        $db = DB::getInstance();
        $request = $db->query('SELECT setting_value FROM settings WHERE setting_key="major"');
        $setted_major = $request->fetch();
        return intval(($setted_major['setting_value']));
    }

    /**
     * Get global major name
     * @return String
     */
    public static function getSettedMajorName() {
        $db = DB::getInstance();
        $request = $db->query('SELECT name FROM majors WHERE id=(SELECT setting_value FROM settings WHERE setting_key="major")');
        $setted_major_name = $request->fetch();
        return $setted_major_name['name'];
    }

    /**
     * Get all available languages
     * @return Array
     */
    public static function getLanguages() {
        return Setting::$languages;
    }
}
?>