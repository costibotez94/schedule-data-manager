<?php

/**
 * Professor Model
 */
class Professor {

    public $id;
    public $grade;
    public $name;

    /**
     * Professors Constructor
     * @param Int $id
     * @param String $name
     * @param Object $grade
     */
    public function __construct($id, $name, $grade) {
        $this->id       = $id;
        $this->name     = $name;
        $this->grade    = $grade;
    }

    /**
     * Get all professors from database
     * @return Array
     */
    public static function getAll() {
        $list = [];
        $db = DB::getInstance();
        $request = $db->query("SELECT p.id 'id', p.name 'name', pg.name 'grade', p.grade_id FROM professors p INNER JOIN professors_grade pg ON p.grade_id = pg.id");

        foreach ($request->fetchAll() as $professor) {
            $list[] = new Professor($professor['id'],
                                    $professor['name'],
                                    (object)['name'    => $professor['grade'],
                                             'id' => $professor['grade_id']]);
        }

        return $list;
    }

    /**
     * Get all professors grades from database
     * @return Array
     */
    public static function getAllGrades() {
        $list = [];
        $db = DB::getInstance();
        $request = $db->query('SELECT * FROM professors_grade');

        foreach ($request->fetchAll(PDO::FETCH_CLASS) as $grade) {
            $list[] = $grade;
        }

        return $list;
    }

    /**
     * Add professor in database
     * @param String $name
     * @param int $grade_id
     */
    public static function add($name, $grade_id) {
        $db = DB::getInstance();

        $request = $db->prepare('INSERT INTO professors(name, grade_id) VALUES (:name, :grade_id)');
        $request->execute(array(
            'name'  => $name,
            'grade_id' => $grade_id
        ));
    }

    /**
     * Delete professor from database
     * @param int $id
     * @return void
     */
    public static function delete($id) {
        $db = DB::getInstance();

        $request = $db->prepare('DELETE FROM professors WHERE id = :id');
        $request->execute(array('id'=>$id));
    }

    /**
     * Update professor from database
     * @param  Int $id
     * @param  String $name
     * @param  String $grade_id
     * @return void
     */
    public static function update($id, $name, $grade_id) {
        $db = DB::getInstance();
        try {
            if($name && $grade_id) {
                $request = $db->prepare('UPDATE professors SET name = :name, grade_id = :grade_id WHERE id = :id');
                $request->execute(array(
                    'name'  => $name,
                    'grade_id' => $grade_id,
                    'id'    => $id
                ));
            } else
            if($name) {
                $request = $db->prepare('UPDATE professors SET name = :name WHERE id = :id');
                $request->execute(array(
                    'name'  => $name,
                    'id'    => $id
                ));
            } else
            if($grade) {
                $request = $db->prepare('UPDATE professors SET grade = :grade WHERE id = :id');
                $request->execute(array(
                    'grade' => $grade,
                    'id'    => $id
                ));
            }
        } catch (Exception $e) {
            echo 'Caught exception:' . $e->getMessage();
        }
    }
}
?>