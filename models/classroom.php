<?php

/**
 * Classroom Model
 */
class Classroom {

    public $id;
	public $name;
	public $size;
    public $type;

    /**
     * Classroom's Constructor
     * @param Int $id
     * @param String $name
     * @param Int $size
     * @param String $type
     */
    public function __construct($id, $name, $size, $type) {
        $this->id   = $id;
        $this->name = $name;
        $this->size = $size;
        $this->type = $type;
    }

    /**
     * Get all classrooms from database
     * @return Array
     */
    public static function getAll() {
        $list = [];
        $db = DB::getInstance();
        $request = $db->query('SELECT c.id, c.name, c.size, ct.name "type" FROM classrooms c INNER JOIN classroom_type ct ON c.type_id = ct.id ORDER BY c.id');

        foreach ($request->fetchAll() as $classroom) {
            $list[] = new Classroom($classroom['id'], $classroom['name'], $classroom['size'], $classroom['type']);
        }

        return $list;
    }

    /**
     * Add classroom in database
     * @param String $name
     * @param Int $size
     * @param Int $type_id
     */
    public static function add($name, $size, $type_id) {
        $db = DB::getInstance();

        $request = $db->prepare('INSERT INTO classrooms(name, size, type_id) VALUES (:name, :size, :type_id)');
        $request->execute(array(
            'name'  => $name,
            'size'  => $size,
            'type_id'  => $type_id
        ));
    }

    /**
     * Delete classroom from database
     * @param int $id
     * @return void
     */
    public static function delete($id) {
        $db = DB::getInstance();

        $request = $db->prepare('DELETE FROM classrooms WHERE id = :id');
        $request->execute(array('id'=>$id));
    }

    /**
     * Update classroom from database
     * @param  Int $id
     * @param  Int $size
     * @param  Int $type_id
     * @return void
     */
    public static function update($id, $name, $size, $type_id) {
        $db = DB::getInstance();
        if(!empty($name)) {
            $request = $db->prepare('UPDATE classrooms SET name = :name WHERE id = :id');
            $request->execute(array(
                'name'  => $name,
                'id'    => $id
            ));
        }
        if(!empty($type_id)) {
            $request = $db->prepare('UPDATE classrooms SET type_id = :type_id WHERE id = :id');
            $request->execute(array(
                'type_id'  => $type_id,
                'id'    => $id
            ));
        }
        if(!empty($size)) {
            $request = $db->prepare('UPDATE classrooms SET size = :size WHERE id = :id');
            $request->execute(array(
                'size' => $size,
                'id'    => $id
            ));
        }
    }

    /**
     * Get all classrooms from database
     * @return Array
     */
    public static function getClassroomsTypes() {
        $list = [];
        $db = DB::getInstance();
        $request = $db->query('SELECT * FROM classroom_type');

        foreach ($request->fetchAll() as $type) {
            $list[] =   array(
                            'id'    => $type['id'],
                            'name'  => $type['name']
                        );
        }
        return $list;
    }

}
?>