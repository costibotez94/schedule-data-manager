<?php

/**
 * Class Model
 */
class Class_ {

    public $id;
	public $professor;
	public $group;
    public $course;
    public $classroom;

    /**
     * Class Constructor
     * @param int $id
     * @param Object(Professor) $professor
     * @param Object(Group) $group
     * @param Object(Course) $course
     * @param Object(Classroom) $classroom
     */
    public function __construct($id, $professor, $group, $course, $classroom) {
        $this->id           = $id;
        $this->professor    = $professor;
        $this->group        = $group;
        $this->course       = $course;
        $this->classroom    = $classroom;
    }

    /**
     * Get all classs from database
     * @return Array
     */
    public static function getAll() {
        $list = [];
        $db = DB::getInstance();
        $request = $db->query(
            'SELECT c.id "id", p.name "professor", g.name "group", co.name "course", cl.name "classroom"
             FROM classes c INNER JOIN professors p on p.id = c.professor_id
                            INNER JOIN groups g     on g.id = c.group_id
                            INNER JOIN courses co    on co.id = c.course_id
                            INNER JOIN classrooms cl on cl.id = c.classroom_id
             ORDER BY c.id');

        foreach ($request->fetchAll() as $class) {
            $list[] = new Class_($class['id'], $class['professor'], $class['group'], $class['course'], $class['classroom']);
        }

        return $list;
    }

    /**
     * Add class in database
     * @param Int $professor_id
     * @param Int $group_id
     * @param Int $course_id
     * @param Int $classroom_id
     */
    public static function add($professor_id, $group_id, $course_id, $classroom_id) {
        $db = DB::getInstance();

        $request = $db->prepare('INSERT INTO classes(professor_id, group_id, course_id, classroom_id) VALUES (:professor_id, :group_id, :course_id, :classroom_id)');
        $request->execute(array(
            'professor_id'  => $professor_id,
            'group_id'  => $group_id,
            'course_id'  => $course_id,
            'classroom_id'  => $classroom_id
        ));
    }

    /**
     * Update class in database
     * @param  Int $id
     * @param  Int $professor_id
     * @param  Int $group_id
     * @param  Int $course_id
     * @param  Int $classroom_id
     * @return void
     */
    public static function update($id, $professor_id, $group_id, $course_id, $classroom_id) {
        $db = DB::getInstance();
        if(!empty($professor_id)) {
            $request = $db->prepare('UPDATE classes SET professor_id = :professor_id WHERE id = :id');
            $request->execute(array(
                'professor_id'  => $professor_id,
                'id'    => $id
            ));
        }
        if(!empty($group_id)) {
            $request = $db->prepare('UPDATE classes SET group_id = :group_id WHERE id = :id');
            $request->execute(array(
                'group_id'  => $group_id,
                'id'    => $id
            ));
        }
        if(!empty($course_id)) {
            $request = $db->prepare('UPDATE classes SET course_id = :course_id WHERE id = :id');
            $request->execute(array(
                'course_id' => $course_id,
                'id'    => $id
            ));
        }
        if(!empty($classroom_id)) {
            $request = $db->prepare('UPDATE classes SET classroom_id = :classroom_id WHERE id = :id');
            $request->execute(array(
                'classroom_id' => $classroom_id,
                'id'    => $id
            ));
        }
    }

    /**
     * Delete class from database
     * @param int $id
     * @return void
     */
    public static function delete($id) {
        $db = DB::getInstance();

        $request = $db->prepare('DELETE FROM classes WHERE id = :id');
        $request->execute(array('id'=>$id));
    }
}
?>