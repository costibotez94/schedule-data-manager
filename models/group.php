<?php

/**
 * Group Model
 */
class Group {

    public $id;
    public $name;
    public $size;
    protected $series_id;
    public $series_name;

    /**
     * Groups Constructor
     * @param Int $id
     * @param String $name
     * @param Int $size
     * @param Int $series_id
     * @param String $series_name
     */
    public function __construct($id, $name, $size, $series_id, $series_name) {
        $this->id           = $id;
        $this->name         = $name;
        $this->size         = $size;
        $this->series_id    = $series_id;
        $this->series_name  = $series_name;
    }

    /**
     * Get all groups from database
     * @return Array
     */
    public static function getAll() {
        $list = [];
        $db = DB::getInstance();
        $request = $db->query('SELECT g.id, g.name, g.size, g.series_id, s.name AS series_name
                               FROM groups g INNER JOIN series s
                               ON s.id=g.series_id');
        foreach ($request->fetchAll() as $group) {
            $list[] = new Group($group['id'], $group['name'], $group['size'], $group['series_id'], $group['series_name']);
        }

        return $list;
    }

    /**
     * Add group in database
     * @param String $name
     * @param Int $size
     * @param Int $series_id
     */
    public static function add($name, $size, $series_id) {
        $db = DB::getInstance();
        $request = $db->prepare('INSERT INTO groups(name, size, series_id) VALUES (:name, :size, :series_id)');
        $request->execute(array(
            'name'      => $name,
            'size'      => $size,
            'series_id' => $series_id
        ));
    }

    /**
     * Delete group from database
     * @param Int $id
     * @return void
     */
    public static function delete($id) {
        $db = DB::getInstance();

        $request = $db->prepare('DELETE FROM groups WHERE id = :id');
        $request->execute(array('id'=>$id));
    }

    /**
     * Update group from database
     * @param Int $id
     * @param Int $size
     * @return void
     */
    public static function update($id, $size) {
        $db = DB::getInstance();
        try {
            if($size) {
                $request = $db->prepare('UPDATE groups SET size = :size WHERE id = :id');
                $request->execute(array(
                    'size'  => $size,
                    'id'    => $id
                ));
            }
        } catch (Exception $e) {
            echo 'Caught exception:' . $e->getMessage();
        }
    }

    /**
     * Get available series from a major
     * @return HTML
     */
    public static function getSeriesForMajor($major_id) {
        $list = [];
        $db = DB::getInstance();
        $request = $db->query('SELECT * FROM series WHERE major_id = '. $major_id);

        foreach ($request->fetchAll() as $serie) {
            $list[] = [$serie['id'], $serie['year'], $serie['name']];
        }
        return $list;
    }

    public static function getAllGroupsForSeries() {
        $list = [];
        $db = DB::getInstance();
        $request = $db->query(' SELECT g.id, g.name, s.name "serie_name", m.name "major_name"
                                FROM groups g INNER JOIN series s ON g.series_id = s.id
                                              INNER JOIN majors m ON s.major_id = m.id ');
        foreach ($request->fetchAll() as $group) {
            $list[] =   array(
                            'id'            => $group['id'],
                            'name'          => $group['name'],
                            'serie_name'    => $group['serie_name'],
                            'major_name'    => $group['major_name']
                        );
        }
        // echo '<pre>'; print_r($list); echo '</pre>';

        return $list;
    }
}
?>