<?php

/**
 * Constraint Model
 */
class Constraint {

    public $constraints;
    public $constraint_id;

    /**
     * Constraint Constructor
     * @param Array $constraints
     * @param Int $constraint_id
     */
    public function __construct($constraints, $constraint_id) {
        $this->constraints  = $constraints;
        $this->constraint_id  = $constraint_id;
    }

    public static function getAllConstraintsIDs() {
        $list = [];
        $db = DB::getInstance();
        $request = $db->query('SELECT * FROM constraints WHERE deleted = 0');

        foreach ($request->fetchAll() as $constraint) {
            $list[] = [$constraint['id'], $constraint['description']];
        }

        return $list;
    }

    /**
     * Get all constraints from database
     * @return Array
     */
    public static function getAll() {
        $list = [];
        $db = DB::getInstance();

        $constraint_ids = Constraint::getAllConstraintsIDs();

        $i=0; $k=0;
        foreach ($constraint_ids as $constraint_id) {
            $constraints = [];
            $request = $db->query(
                'SELECT professor_id "Professor", classroom_id "Classroom", group_id "Group", course_id "Course", major_id "Major", from_date, to_date, from_hour, to_hour, weekdays FROM constraint_item WHERE constraint_id = '. $constraint_id[0] . ' AND deleted = 0 ORDER BY start ASC');

            foreach ($request->fetchAll(PDO::FETCH_CLASS) as $constraint) {
                foreach ($constraint as $key => $prop) {
                    if(!empty($prop)) {
                        $constraints[$k][$i] = [$key, $prop];
                        $i++;
                    }

                    if($i%2==0 && $i!=0) {
                        $k++;
                        $i=0;
                    }
                }
            }
            $list[] = new Constraint( $constraints, $constraint_id[1] );
        }
            // echo '<pre>'; print_r($list); echo '</pre>';

        return $list;
    }

    public static function getAvailableConstraints() {
        return ['Professor', 'Classroom', 'Group', 'Course', 'Major', 'Date Interval', 'Hour Interval'];
    }

    /**
     * Add constraint in database
     * @param Array $constraints
     */
    public static function add($constraints) {
        $db = DB::getInstance();

        foreach ($constraints as $constraint) {
            $start = 0;
            foreach ($constraint as $key => $value) {
                switch ($key) {
                    case 'professor':
                        $request = $db->prepare('INSERT INTO constraint_item(professor_id, constraint_id, start, deleted) VALUES (:professor_id, :constraint_id, :start, 0)');
                        $request->execute(array(
                            'professor_id'      => $value,
                            'constraint_id'     => $constraint['action'],
                            'start'             => (Constraint::getMaxStart($constraint['action'])+1)
                        ));
                        break;
                    case 'classroom':
                        $request = $db->prepare('INSERT INTO constraint_item(classroom_id, constraint_id, start, deleted) VALUES (:classroom_id, :constraint_id, :start, 0)');
                        $request->execute(array(
                            'classroom_id'      => $value,
                            'constraint_id'     => $constraint['action'],
                            'start'             => (Constraint::getMaxStart($constraint['action'])+1)
                        ));
                        break;
                    case 'major':
                        $request = $db->prepare('INSERT INTO constraint_item(major_id, constraint_id, start, deleted) VALUES (:major_id, :constraint_id, :start, 0)');
                        $request->execute(array(
                            'major_id'          => $value,
                            'constraint_id'     => $constraint['action'],
                            'start'             => (Constraint::getMaxStart($constraint['action'])+1)
                        ));
                        break;
                    case 'group':
                        $request = $db->prepare('INSERT INTO constraint_item(group_id, constraint_id, start, deleted) VALUES (:group_id, :constraint_id, :start, 0)');
                        $request->execute(array(
                            'group_id'          => $value,
                            'constraint_id'     => $constraint['action'],
                            'start'             => (Constraint::getMaxStart($constraint['action'])+1)
                        ));
                        break;
                    case 'course':
                        $request = $db->prepare('INSERT INTO constraint_item(course_id, constraint_id, start, deleted) VALUES (:course_id, :constraint_id, :start, 0)');
                        $request->execute(array(
                            'course_id'      => $value,
                            'constraint_id'     => $constraint['action'],
                            'start'             => (Constraint::getMaxStart($constraint['action'])+1)
                        ));
                        break;
                    case 'date_interval':
                        $request = $db->prepare('INSERT INTO constraint_item(from_date, to_date, constraint_id, start, deleted) VALUES (:from_date, :to_date, :constraint_id, :start, 0)');
                        $request->execute(array(
                            'from_date'         => $value['from_date'],
                            'to_date'           => $value['to_date'],
                            'constraint_id'     => $constraint['action'],
                            'start'             => (Constraint::getMaxStart($constraint['action'])+1)
                        ));
                        break;
                    case 'hour_interval':
                        $request = $db->prepare('INSERT INTO constraint_item(from_hour, to_hour, constraint_id, start, deleted) VALUES (:from_hour, :to_hour, :constraint_id, :start, 0)');
                        $request->execute(array(
                            'from_hour'         => $value['from_hour'],
                            'to_hour'           => $value['to_hour'],
                            'constraint_id'     => $constraint['action'],
                            'start'             => (Constraint::getMaxStart($constraint['action'])+1)
                        ));
                        break;
                }
            }
        }
    }

    public static function getMaxStart($constraint_id) {
        $list = [];
        $db = DB::getInstance();

        $request = $db->query('SELECT max(start) "constraint_id" FROM constraint_item WHERE constraint_id = '. $constraint_id . ' AND deleted = 0');

        return $request->fetch()['constraint_id'];
    }

    /**
     * Delete constraint from database
     * @param Int $id
     * @return void
     */
    public static function delete($id) {
        $db = DB::getInstance();

        $request = $db->prepare('UPDATE constraints_item SET deleted = 1  WHERE constraint_id = :id');
        $request->execute(array('id'=>$id));
    }

    /**
     * Update constraint from database
     * @param  Int $id
     * @param  String $description
     */
    public static function update($id, $description) {
        $db = DB::getInstance();
        if(!empty($description)) {
            $request = $db->prepare('UPDATE constraints SET description = :description WHERE id = :id');
            $request->execute(array(
                'description'   => $description,
                'id'            => $id
            ));
        }
    }
}
?>