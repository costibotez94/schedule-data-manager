<?php

/**
 * Major Model
 */
class Major {

    public $id;
	public $name;

    /**
     * Majors Constructor
     * @param Int $id
     * @param String $name
     */
    public function __construct($id, $name) {
        $this->id   = $id;
        $this->name = $name;
    }

    /**
     * Get all majors from database
     * @return Array
     */
    public static function getAll() {
        $list = [];
        $db = DB::getInstance();
        $request = $db->query('SELECT * FROM majors');

        foreach ($request->fetchAll() as $major) {
            $list[] = new Major($major['id'], $major['name']);
        }

        return $list;
    }

    /**
     * Add major in database
     * @param String $name
     */
    public static function add($name) {
        $db = DB::getInstance();

        $request = $db->prepare('INSERT INTO majors(name) VALUES (:name)');
        $request->execute(array(
            'name'  => $name
        ));
    }

    /**
     * Delete major from database
     * @param int $id
     * @return void
     */
    public static function delete($id) {
        $db = DB::getInstance();

        $request = $db->prepare('DELETE FROM majors WHERE id = :id');
        $request->execute(array('id'=>$id));
    }

    /**
     * Update major from database
     * @param  Int $id
     * @param  String $name
     * @return void
     */
    public static function update($id, $name) {
        $db = DB::getInstance();
        if(!empty($name)) {
            $request = $db->prepare('UPDATE majors SET name = :name WHERE id = :id');
            $request->execute(array(
                'name'  => $name,
                'id'    => $id
            ));
        }
    }
}
?>