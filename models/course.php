<?php

/**
 * Course Model
 */
class Course {

    public $id;
    public $name;
    public $evaluation;
    public $type;
    public $semester;
    public $year;
    private $major_id;
    public $major_name;
    public $credits;

    /**
     * Course Constructor
     * @param Int $id
     * @param String $name
     * @param String $evaluation
     * @param String $type
     * @param Int $semester
     * @param Int $year
     * @param Int $major_id
     * @param String $major_name
     * @param Int credits
     */
    public function __construct($id, $name, $evaluation, $type, $semester, $year, $major_id, $major_name, $credits) {
        $this->id           = $id;
        $this->name         = $name;
        $this->evaluation   = $evaluation;
        $this->type         = $type;
        $this->semester     = $semester;
        $this->year         = $year;
        $this->major_id     = $major_id;
        $this->major_name   = $major_name;
        $this->credits      = $credits;
    }

    /**
     * Get all courses from database
     * @return Array
     */
    public static function getAll() {
        $list = [];
        $db = DB::getInstance();
        $request = $db->query('SELECT c.id, c.name, c.evaluation, c.type, c.semester, c.year, c.major_id, c.credits, m.name AS major_name
                               FROM courses c INNER JOIN majors m
                               ON c.major_id=m.id');
        foreach ($request->fetchAll() as $course) {
            $list[] = new Course($course['id'], $course['name'], $course['evaluation'], $course['type'], $course['semester'], $course['year'], $course['major_id'], $course['major_name'], $course['credits']);
        }

        return $list;
    }

    /**
     * Add course in database
     * @param String $name
     * @param String $evaluation
     * @param String $type
     * @param Int $semester
     * @param Int $year
     * @param Int $major_id
     * @param Int $credits
     * @param Array $credits
     */
    public static function add($name, $evaluation, $type, $semester, $year, $major_id, $credits, $professors) {
        $db = DB::getInstance();

        $request = $db->prepare('INSERT INTO courses(name, evaluation, type, semester, year, major_id, credits) VALUES (:name, :evaluation, :type, :semester, :year, :major_id, :credits)');
        $request->execute(array(
            'name'          => $name,
            'evaluation'    => $evaluation,
            'type'          => $type,
            'semester'      => $semester,
            'year'          => $year,
            'major_id'      => $major_id,
            'credits'       => $credits
        ));

        $course = $db->query("SELECT max(id) 'id' FROM courses");
        $course_id = $course->fetch(PDO::FETCH_ASSOC)['id'];

        foreach ($professors as $key => $professor_id) {
            $request = $db->prepare('INSERT INTO course_professors(course_id, professor_id) VALUES (:course_id, :professor_id)');
            $request->execute(array(
                'course_id'     => $course_id,
                'professor_id'  => $professor_id
            ));
        }
    }

    /**
     * Delete course from database
     * @param int $id
     * @return void
     */
    public static function delete($id) {
        $db = DB::getInstance();

        $request = $db->prepare('DELETE FROM courses WHERE id = :id');
        $request->execute(array('id'=>$id));
    }

    /**
     * Update course from database
     * @param Int $id
     * @param String $evaluation
     * @param String $type
     * @param Int $semester
     * @param Int $year
     * @param Int $major_id
     * @param Int $credits
     * @return void
     */
    public static function update($id, $evaluation, $type, $semester, $year, $major_id, $credits) {
        $db = DB::getInstance();
        try {
            if($evaluation) {
                $request = $db->prepare('UPDATE courses SET evaluation = :evaluation WHERE id = :id');
                $request->execute(array(
                    'evaluation'  => $evaluation,
                    'id'    => $id
                ));
            }
            if($type) {
                $request = $db->prepare('UPDATE courses SET type = :type WHERE id = :id');
                $request->execute(array(
                    'type'  => $type,
                    'id'    => $id
                ));
            }
            if($semester) {
                $request = $db->prepare('UPDATE courses SET semester = :semester WHERE id = :id');
                $request->execute(array(
                    'semester'  => $semester,
                    'id'    => $id
                ));
            }
            if($year) {
                $request = $db->prepare('UPDATE courses SET year = :year WHERE id = :id');
                $request->execute(array(
                    'year'  => $year,
                    'id'    => $id
                ));
            }
            if($major_id) {
                $request = $db->prepare('UPDATE courses SET major_id = :major_id WHERE id = :id');
                $request->execute(array(
                    'major_id'  => $major_id,
                    'id'    => $id
                ));
            }
            if($credits) {
                $request = $db->prepare('UPDATE courses SET credits = :credits WHERE id = :id');
                $request->execute(array(
                    'credits'  => $credits,
                    'id'    => $id
                ));
            }
            $request->closeCursor();
        } catch (Exception $e) {
            echo 'Caught exception:' . $e->getMessage();
        }
    }
}
?>