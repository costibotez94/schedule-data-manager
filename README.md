# Schedule Data Manager #

The web-app is a management system of academic schedules and is composed by 8 modules, 8 controllers, 38 views and a general settings panel that works by MVC pattern. Each controller communicates with his own module and view independently according to the user's action on the system.
The wee-app is written using languages such as HTML, CSS, PHP, Javascript, MySQL and frameworks such as BootStrap, jQuery, Chosen. For management I used Asana software.