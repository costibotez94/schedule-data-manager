<?php

/**
 * Groups Controller
 */
class GroupsController {
	/**
	 * Show all groups and see witch callback trigger the action
	 * @return Array
	 */
	public function show() {
		if(isset($_POST['form']) && !empty($_POST['form'])) {
			switch ($_POST['form']) {
				case 'add-group':
					if(isset($_POST['name']) && !empty($_POST['name']) &&
					   isset($_POST['size']) && !empty($_POST['size']) &&
					   isset($_POST['series_id']) && !empty($_POST['series_id'])) {
						$name 		= strip_tags(trim($_POST['name']));
						$size  		= strip_tags(trim($_POST['size']));
						$series_id  = strip_tags(trim($_POST['series_id']));
						Group::add($name, $size, $series_id);
					}
					break;
				case 'update-group':
					if(isset($_POST['id']) && !empty($_POST['id']) &&
				       isset($_POST['size']) && !empty($_POST['size'])) {
						Group::update($_POST['id'], $_POST['size']);
					}
					break;
				case 'delete-group':
					if(isset($_POST['id']) && !empty($_POST['id'])) {
						Group::delete($_POST['id']);
					}
					break;
			}
		}
		$groups = Group::getAll();
		require_once('views/groups/index.php');
	}
	/**
	 * Add group
	 * @return void
	 */
	public function add() {
		$major_id	= Setting::getSettedMajorID();
		$major_name	= Setting::getSettedMajorName();
		$series 	= Group::getSeriesForMajor($major_id);
		require_once('views/groups/add.php');
	}

	/**
	 * Delete group
	 * @return void
	 */
	public function delete() {
		$groups 	= Group::getAllGroupsForSeries();
		require_once('views/groups/delete.php');
	}

	/**
	 * Update group
	 * @return void
	 */
	public function update() {
		$groups 	= Group::getAllGroupsForSeries();
		require_once('views/groups/update.php');
	}
}

?>