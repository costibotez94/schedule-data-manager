<?php

/**
 * Majors Controller
 */
class MajorsController {
	/**
	 * Show all majors
	 * @return Array
	 */
	public function show() {
		if(isset($_POST['form']) && !empty($_POST['form'])) {
			switch ($_POST['form']) {
				case 'add-major':
					if(isset($_POST['name']) && !empty($_POST['name'])) {
						$name = strip_tags(trim($_POST['name']));
						Major::add($name);
					}
				break;
				case 'update-major':
					if(isset($_POST['id']) && !empty($_POST['id']) &&
					   isset($_POST['name']) && !empty($_POST['name'])) {
						Major::update($_POST['id'], $_POST['name']);
					}
				break;
				case 'delete-major':
					if(isset($_POST['id']) && !empty($_POST['id'])) {
						Major::delete($_POST['id']);
					}
				break;
			}
		}
		$majors = Major::getAll();
		require_once('views/majors/index.php');
	}
	/**
	 * Add major
	 * @return void
	 */
	public function add() {
		require_once('views/majors/add.php');
	}

	/**
	 * Delete major
	 * @return void
	 */
	public function delete() {
		$majors = Major::getAll();
		require_once('views/majors/delete.php');
	}

	/**
	 * Update major
	 * @return void
	 */
	public function update() {
		$majors = Major::getAll();
		require_once('views/majors/update.php');
	}
}

?>