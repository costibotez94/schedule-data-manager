<?php

/**
 * Courses Controller
 */
class CoursesController {
	/**
	 * Show all courses and see witch callback trigger the action
	 * @return Array
	 */
	public function show() {
		if(isset($_POST['form']) && !empty($_POST['form'])) {
			switch ($_POST['form']) {
				case 'add-course':
					if(isset($_POST['name']) && !empty($_POST['name']) &&
					   isset($_POST['evaluation']) && !empty($_POST['evaluation']) &&
					   isset($_POST['type']) && !empty($_POST['type']) &&
					   isset($_POST['semester']) && !empty($_POST['semester']) &&
					   isset($_POST['year']) && !empty($_POST['year']) &&
					   isset($_POST['major_id']) && !empty($_POST['major_id']) &&
					   isset($_POST['professors']) && !empty($_POST['professors'])) {
						$name 			= strip_tags(trim($_POST['name']));
						$evaluation 	= strip_tags(trim($_POST['evaluation']));
						$type 			= strip_tags(trim($_POST['type']));
						$semester 		= strip_tags(trim($_POST['semester']));
						$year 			= strip_tags(trim($_POST['year']));
						$major_id 		= strip_tags(trim($_POST['major_id']));
						$credits		= strip_tags(trim($_POST['credits']));
						$professors		= $_POST['professors'];
						Course::add($name, $evaluation, $type, $semester, $year, $major_id, $credits, $professors);
					}
				break;
				case 'update-course':
					if(isset($_POST['id']) && !empty($_POST['id']) &&
					   (isset($_POST['evaluation']) && !empty($_POST['evaluation']) ||
					   isset($_POST['type']) && !empty($_POST['type']) ||
					   isset($_POST['semester']) && !empty($_POST['semester']) ||
					   isset($_POST['year']) && !empty($_POST['year']) ||
					   isset($_POST['major_id']) && !empty($_POST['major_id']) ||
					   isset($_POST['credits']) && !empty($_POST['credits']))) {
						Course::update($_POST['id'], $_POST['evaluation'], $_POST['type'], $_POST['semester'], $_POST['year'], $_POST['major_id'], $_POST['credits']);
					}
				break;
				case 'delete-course':
					if(isset($_POST['id']) && !empty($_POST['id'])) {
						Course::delete($_POST['id']);
					}
				break;
			}
		}
		$courses = Course::getAll();
		require_once('views/courses/index.php');
	}
	/**
	 * Add course
	 * @return void
	 */
	public function add() {
		$majors 	= Major::getAll();
		$professors = Professor::getAll();
		require_once('views/courses/add.php');
	}

	/**
	 * Delete course
	 * @return void
	 */
	public function delete() {
		$courses 	= Course::getAll();
		require_once('views/courses/delete.php');
	}

	/**
	 * Update course
	 * @return void
	 */
	public function update() {
		$courses 	= Course::getAll();
		$majors 	= Major::getAll();
		require_once('views/courses/update.php');
	}
}

?>