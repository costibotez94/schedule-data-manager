<?php

/**
 * Constraints Controller
 */
class ConstraintsController {
	/**
	 * Show all courses and see witch callback trigger the action
	 * @return Array
	 */
	public function show() {
		$constraint 	= [];

		if(isset($_POST['form']) && !empty($_POST['form'])) {
			switch ($_POST['form']) {
				case 'add-constraint':
					// echo '<pre>'; print_r($_POST); echo '</pre>';
					// var_dump($_POST['constraint_1'][0] != '0');
					// var_dump(isset($_POST['constraint_1']) && !empty($_POST['constraint_1'][0]) && $_POST['constraint_1'][0] !=0);
					if(isset($_POST['constraint_1']) && !empty($_POST['constraint_1'][0]) && $_POST['constraint_1'][0] !='0' &&
					   isset($_POST['constraint_2']) && !empty($_POST['constraint_2'][0]) && $_POST['constraint_2'][0] !='0') {
						for ($i=0; $i<count($_POST['constraint_1']); $i++) {
							switch ($_POST['constraint_1'][$i]) {
								case 'Professor':
									if(isset($_POST['professor_1'][$i]) && !empty($_POST['professor_1'][$i])) {
										$constraint[$i]['professor'] = $_POST['professor_1'][$i];
										$constraint[$i]['action'] = $_POST['action'][$i];
									}
								break;
								case 'Classroom':
									if(isset($_POST['classroom_1'][$i]) && !empty($_POST['classroom_1'][$i])) {
										$constraint[$i]['classroom'] = $_POST['classroom_1'][$i];
										$constraint[$i]['action'] = $_POST['action'][$i];
									}
								break;
								case 'Group':
									if(isset($_POST['group_1'][$i]) && !empty($_POST['group_1'][$i])) {
										$constraint[$i]['group'] = $_POST['group_1'][$i];
										$constraint[$i]['action'] = $_POST['action'][$i];
									}
								break;
								case 'Course':
									if(isset($_POST['course_1'][$i]) && !empty($_POST['course_1'][$i])) {
										$constraint[$i]['course'] = $_POST['course_1'][$i];
										$constraint[$i]['action'] = $_POST['action'][$i];
									}
								break;
								case 'Major':
									if(isset($_POST['major_1'][$i]) && !empty($_POST['major_1'][$i])) {
										$constraint[$i]['major'] = $_POST['major_1'][$i];
										$constraint[$i]['action'] = $_POST['action'][$i];
									}
								break;
								case 'Date Interval':
									if(isset($_POST['date_interval_1'][$i]) && !empty($_POST['date_interval_1'][$i])) {
										$constraint[$i]['date_interval'] = $_POST['date_interval_1'][$i];
										$constraint[$i]['action'] = $_POST['action'][$i];
									}
								break;
								case 'Hour Interval':
									if(isset($_POST['hour_interval_1'][$i]) && !empty($_POST['hour_interval_1'][$i])) {
										$constraint[$i]['hour_interval'] = $_POST['hour_interval_1'][$i];
										$constraint[$i]['action'] = $_POST['action'][$i];
									}
								break;
							}
							switch ($_POST['constraint_2'][$i]) {
								case 'Professor':
									if(isset($_POST['professor_2'][$i]) && !empty($_POST['professor_2'][$i])) {
										$constraint[$i]['professor'] = $_POST['professor_2'][$i];
									}
								break;
								case 'Classroom':
									if(isset($_POST['classroom_2'][$i]) && !empty($_POST['classroom_2'][$i])) {
										$constraint[$i]['classroom'] = $_POST['classroom_2'][$i];
									}
								break;
								case 'Group':
									if(isset($_POST['group_2'][$i]) && !empty($_POST['group_2'][$i])) {
										$constraint[$i]['group'] = $_POST['group_2'][$i];
									}
								break;
								case 'Course':
									if(isset($_POST['course_2'][$i]) && !empty($_POST['course_2'][$i])) {
										$constraint[$i]['course'] = $_POST['course_2'][$i];
									}
								break;
								case 'Major':
									if(isset($_POST['major_2'][$i]) && !empty($_POST['major_2'][$i])) {
										$constraint[$i]['major'] = $_POST['major_2'][$i];
									}
								break;
								case 'Date Interval':
									if(isset($_POST['date_interval_2'][$i]) && !empty($_POST['date_interval_2'][$i])) {
										$constraint[$i]['date_interval'] = $_POST['date_interval_2'][$i];
									}
								break;
								case 'Hour Interval':
									if(isset($_POST['hour_interval_2'][$i]) && !empty($_POST['hour_interval_2'][$i])) {
										$constraint[$i]['hour_interval'] = $_POST['hour_interval_2'][$i];
									}
								break;
							}
						}
						// echo '<pre>'; print_r($constraint); echo '</pre>';

						Constraint::add($constraint);
					}
				break;
				case 'delete-constraint':
					echo '<pre>'; print_r($_POST); echo '</pre>';

					Constraint::delete($_POST['id']);
				break;
			}
		}

		// echo '<pre>'; print_r($constraint); echo '</pre>';
		$constraints 			= Constraint::getAll();
		$professors 			= Professor::getAll();
		$groups					= Group::getAll();
		$majors					= Major::getAll();
		$courses				= Course::getAll();
		$classrooms				= Classroom::getAll();
		$available_constraints 	= Constraint::getAvailableConstraints();
		$available_actions		= Constraint::getAllConstraintsIDs();
		require_once('views/constraints/index.php');
	}
	/**
	 * Add constraint
	 * @return void
	 */
	public function add() {
		$professors 			= Professor::getAll();
		$groups					= Group::getAll();
		$majors					= Major::getAll();
		$courses				= Course::getAll();
		$classrooms				= Classroom::getAll();
		$available_constraints 	= Constraint::getAvailableConstraints();
		$available_actions		= Constraint::getAllConstraintsIDs();
		$constraints = Constraint::getAll();
		require_once('views/constraints/add.php');
	}

	/**
	 * Delete constraint
	 * @return void
	 */
	public function delete() {
		require_once('views/constraints/delete.php');
	}

	/**
	 * Update constraint
	 * @return void
	 */
	public function update() {
		$constraints = Constraint::getAll();
		require_once('views/constraints/update.php');
	}
}

?>