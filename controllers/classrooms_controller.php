<?php

/**
 * Classrooms Controller
 */
class ClassroomsController {
	/**
	 * Show all classrooms and see witch callback trigger the action
	 * @return Array
	 */
	public function show() {
		if(isset($_POST['form']) && !empty($_POST['form'])) {
			switch ($_POST['form']) {
				case 'add-classroom':
					if(isset($_POST['name']) && !empty($_POST['name']) &&
					   isset($_POST['size']) && !empty($_POST['size']) &&
					   isset($_POST['type']) && !empty($_POST['type'])) {
					   	// echo '<pre>'; print_r($_POST); echo '</pre>';
						$name = strip_tags(trim($_POST['name']));
						$size = strip_tags(trim($_POST['size']));
						$type = strip_tags(trim($_POST['type']));
						Classroom::add($name, $size, $type);
					}
				break;
				case 'delete-classroom':
					if(isset($_POST['id']) && !empty($_POST['id'])) {
						Classroom::delete($_POST['id']);
					}
				break;
				case 'update-classroom':
					if(isset($_POST['id']) && !empty($_POST['id']) &&
					   (isset($_POST['name']) && !empty($_POST['name']) ||
					   isset($_POST['size']) && !empty($_POST['size']) ||
					   isset($_POST['type_id']) && !empty($_POST['type_id']))) {
						Classroom::update($_POST['id'], $_POST['name'], $_POST['size'], $_POST['type_id']);
					}
				break;
			}
		}
		$classrooms = Classroom::getAll();
		require_once('views/classrooms/index.php');
	}
	/**
	 * Add classroom
	 * @return void
	 */
	public function add() {
		$classrooms_type 	= Classroom::getClassroomsTypes();
		require_once('views/classrooms/add.php');
	}

	/**
	 * Delete classroom
	 * @return void
	 */
	public function delete() {
		$classrooms = Classroom::getAll();
		require_once('views/classrooms/delete.php');
	}

	/**
	 * Update classroom
	 * @return void
	 */
	public function update() {
		$classrooms 		= Classroom::getAll();
		$classrooms_type 	= Classroom::getClassroomsTypes();
		require_once('views/classrooms/update.php');
	}
}

?>