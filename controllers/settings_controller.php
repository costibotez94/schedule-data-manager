<?php

/**
 * Settings Controller
 */
class SettingsController {
	/**
	 * Show all settings
	 * @return Array
	 */
	public function show() {
		if(isset($_POST['form']) && $_POST['form'] == 'update-setting') {
			if(isset($_POST['setting_key']) && !empty($_POST['setting_key']) &&
			   isset($_POST['setting_value']) && !empty($_POST['setting_value'])) {
				$setting_key 	= strip_tags(trim($_POST['setting_key']));
				$setting_value  = strip_tags(trim($_POST['setting_value']));
				Setting::update($setting_key, $setting_value);
			}
		}
		$majors 	= Major::getAll();
		$settings 	= Setting::getAll();
		$major_id	= Setting::getSettedMajorID();
		$languages	= Setting::getLanguages();
		require_once('views/settings/index.php');
	}
}

?>