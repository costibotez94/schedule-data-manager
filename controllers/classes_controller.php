<?php

/**
 * Classes Controller
 */
class ClassesController {
	/**
	 * Show all Classes and see witch callback trigger the action
	 * @return Array
	 */
	public function show() {
		if(isset($_POST['form']) && !empty($_POST['form'])) {
			switch ($_POST['form']) {
				case 'add-class':
					if(isset($_POST['professor']) && !empty($_POST['professor']) &&
					   isset($_POST['group']) && !empty($_POST['group']) &&
					   isset($_POST['course']) && !empty($_POST['course']) &&
					   isset($_POST['classroom']) && !empty($_POST['classroom'])) {
					   	// echo '<pre>'; print_r($_POST); echo '</pre>';
						$professor 	= strip_tags(trim($_POST['professor']));
						$group 		= strip_tags(trim($_POST['group']));
						$course 	= strip_tags(trim($_POST['course']));
						$classroom 	= strip_tags(trim($_POST['classroom']));

						Class_::add($professor, $group, $course, $classroom);
					}
				break;
				case 'delete-class':
					if(isset($_POST['id']) && !empty($_POST['id'])) {
						Class_::delete($_POST['id']);
					}
				break;
				case 'update-class':
					if(isset($_POST['id']) && !empty($_POST['id']) &&
					   (isset($_POST['professor_id']) && !empty($_POST['professor_id']) ||
					   isset($_POST['group_id']) && !empty($_POST['group_id']) ||
					   isset($_POST['course_id']) && !empty($_POST['course_id']) ||
					   isset($_POST['classroom_id']) && !empty($_POST['classroom_id']))) {
						Class_::update($_POST['id'], $_POST['professor_id'], $_POST['group_id'], $_POST['course_id'], $_POST['classroom_id']);
					}
				break;
			}
		}

		$professors = Professor::getAll();
		$courses	= Course::getAll();
		$classrooms = Classroom::getAll();
		$groups		= Group::getAll();
		$classes 	= Class_::getAll();
		require_once('views/classes/index.php');
	}

	public function add() {
		$professors = Professor::getAll();
		$courses	= Course::getAll();
		$classrooms = Classroom::getAll();
		$groups		= Group::getAllGroupsForSeries();
		require_once('views/classes/add.php');
	}

	public function delete() {
		$classes 	= Class_::getAll();
		require_once('views/classes/delete.php');
	}

	public function update() {
		$professors = Professor::getAll();
		$courses	= Course::getAll();
		$classrooms = Classroom::getAll();
		$groups		= Group::getAllGroupsForSeries();
		$classes 	= Class_::getAll();
		require_once('views/classes/update.php');
	}
}

?>