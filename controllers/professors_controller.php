<?php

/**
 * Professors Controller
 */
class ProfessorsController {
	/**
	 * Show all professors and see witch callback trigger the action
	 * @return Array
	 */
	public function show() {
		if(isset($_POST['form']) && !empty($_POST['form'])) {
			switch ($_POST['form']) {
				case 'add-professor':
					if(isset($_POST['name']) && !empty($_POST['name']) &&
					   isset($_POST['grade']) && !empty($_POST['grade'])) {
						$name = strip_tags(trim($_POST['name']));
						$grade = strip_tags(trim($_POST['grade']));
						Professor::add($name, $grade);
					}
				break;
				case 'delete-professor':
					if(isset($_POST['id']) && !empty($_POST['id'])) {
						Professor::delete($_POST['id']);
					}
				break;
				case 'update-professor':
					if(isset($_POST['id']) && !empty($_POST['id']) &&
					   (isset($_POST['name']) && !empty($_POST['name']) ||
					   isset($_POST['grade']) && !empty($_POST['grade_id']))) {
						Professor::update($_POST['id'], $_POST['name'], $_POST['grade_id']);
					}
				break;
			}
		}
		$professors = Professor::getAll();
		require_once('views/professors/index.php');
	}
	/**
	 * Add professor
	 * @return void
	 */
	public function add() {
		$grades = Professor::getAllGrades();
		require_once('views/professors/add.php');
	}

	/**
	 * Delete professor
	 * @return void
	 */
	public function delete() {
		$professors = Professor::getAll();
		require_once('views/professors/delete.php');
	}

	/**
	 * Update profesor
	 * @return void
	 */
	public function update() {
		$professors = Professor::getAll();
		$grades 	= Professor::getAllGrades();
		require_once('views/professors/update.php');
	}
}

?>