<?php ini_set('display_errors', 1); ?>
<?php
	// Include MySQL Connection
	require_once('connection.php');

	// Conntroller's validation
	if (isset($_GET['controller']) && isset($_GET['action'])) {
		$controller = $_GET['controller'];
		$action     = $_GET['action'];
	} else {
		$controller = 'pages';
		$action     = 'home';
	}

	// Main View
	require_once('views/layout.php');
?>
