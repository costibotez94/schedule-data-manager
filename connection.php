<?php

/**
 * Database Class - Singleton Class
 */
class DB {
	private static $instance = NULL;

	/**
	 * Singleton constructor
	 * @return Object
	 */
	public static function getInstance() {
		if(!isset(self::$instance)) {
        	self::$instance = new PDO('mysql:host=localhost;dbname=licenta;charset=utf8', 'licenta_usr', 'licenta');
    	}
    	return self::$instance;
	}
}

?>