;(function($){
	$(document).ready(function(){
		$('.panel-danger.add-constraint').on('change', 'select[name^="constraint_"]', function() {
			console.log('asda');
			var selected = $(this).val();
			var constraint = '';
			if ($(this).closest('td').not(':visible')) {
				$(this).closest('td').next('td').show();
			}
			$(this).closest('td').next('td').children().hide();
			switch(selected) {
				case 'Professor':
				// constraint = $(this).closest('tr').find('.hidden select[name="professor[]"]');
				$(this).closest('td').next('td').find('[name^="professor"]').show();
				break;
				case 'Group':
				// constraint = $(this).closest('tr').find('.hidden select[name="group[]"]');
				$(this).closest('td').next('td').find('[name^="group"]').show();
				break;
				case 'Classroom':
				// constraint = $(this).closest('tr').find('.hidden select[name="classroom[]"]');
				$(this).closest('td').next('td').find('[name^="classroom"]').show();
				break;
				case 'Major':
				// constraint = $(this).closest('tr').find('.hidden select[name="major[]"]');
				$(this).closest('td').next('td').find('[name^="major"]').show();
				break;
				case 'Course':
				// constraint = $(this).closest('tr').find('.hidden select[name="course[]"]');
				$(this).closest('td').next('td').find('[name^="course"]').show();
				break;
				// case 'Date Interval':
				// constraint = $(this).closest('tr').find('.hidden select[name=""]');
				// break;
				// case 'Hour Interval':
				// constraint = $(this).closest('tr').find('.hidden select[name=""]');
				// break;
			}
			// var test = 'a';
			// $('.chosen_constraint_1').empty()
			// 						 .append(constraint);
		});

		$('.panel-danger.add-constraint').on('click', '.add-rule', function(e) {
			e.preventDefault();

			var container = $('tbody tr:last-child').clone();
			// container.find('[name^="constraint_"]').val('');
			container.find('td[class^="constraint_"]').hide();
			// container.find('.chosen_constraint_2').val('');
			$(this).closest('tbody').append(container);
		});

		$('[class*="edit-"] select[name="id"]').on('change', function() {

			id 		= $(this).val();
			db_name	= $(this).data('ajax');
			var _this = $(this);
			if (id==0)
			{
				var fields = $(this).data('fields').split(',');

				for(i=0; i<fields.length; i++) {
					fields[i] = fields[i].split(':');

					val = (fields[i][0] == 'select' ? 0 : '');
					$(fields[i][0] + '[name="' + fields[i][1] + '"]').val(val);
				}
				return;
			}

			$.ajax({
	            type: "GET",
	            url: "ajax.php",
	            data: { value : id, db: db_name },
	            success: function(data){
	                // Parse the returned json data
	                var fields = _this.data('fields').split(',');

	                if(data != '') {
		                var data = 	$.parseJSON(data);
						// console.log(data);

		                for(i=0; i<fields.length; i++) {
							fields[i] = fields[i].split(':');
							// console.log(fields[i][1]);
							for (var name in data)
								if(fields[i][1] == name)
								{
									$(fields[i][0] + '[name="' + fields[i][1] + '"]').val(data[name]);
									console.log(name);
								}
						}



			        } else {
			        	for(i=0; i<fields.length; i++) {
							fields[i] = fields[i].split(':');
							val = (fields[i][0] == 'select' ? 0 : '');
							$(fields[i][0] + '[name="' + fields[i][1] + '"]').val(val);
						}
			        }
	            }
	        });
		});

        $('.chosen-select').chosen();
	});
})(jQuery);