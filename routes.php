<?php

/**
 * Controller's callback function
 * @param  String $controller Controller's slug
 * @param  String $action     Method's slud
 * @return void
 */
function call($controller, $action) {
	require_once('controllers/' . $controller . '_controller.php');
	switch ($controller) {
		case 'pages':
			$controller = new PagesController();
			break;
		case 'majors':
			// Include Major Model
			require_once('models/major.php');
			$controller = new MajorsController();
			break;
		case 'professors':
			// Include Professor Model
			require_once('models/professor.php');
			$controller = new ProfessorsController();
			break;
		case 'groups':
			// Include Major and Group Model
			require_once('models/setting.php');
			require_once('models/group.php');
			$controller = new GroupsController();
			break;
		case 'courses':
			// Include Course Model
			require_once('models/course.php');
			require_once('models/major.php');
			require_once('models/professor.php');
			$controller = new CoursesController();
			break;
		case 'classrooms':
			// Include Classroom Model
			require_once('models/classroom.php');
			$controller = new ClassroomsController();
			break;
		case 'constraints':
			// Include Constraint Model
			require_once('models/classroom.php');
			require_once('models/professor.php');
			require_once('models/course.php');
			require_once('models/major.php');
			require_once('models/group.php');
			require_once('models/constraint.php');
			$controller = new ConstraintsController();
			break;
		case 'settings':
			// Include Major and Setting Model
			require_once('models/major.php');
			require_once('models/setting.php');
			$controller = new SettingsController();
			break;
		case 'classes':
			require_once('models/classroom.php');
			require_once('models/professor.php');
			require_once('models/course.php');
			require_once('models/group.php');
			require_once('models/class.php');
			$controller = new ClassesController();
			break;
		case 'schedule':
			// require_once('models/class.php');
			require_once('models/constraint.php');
			$controller = new ScheduleController();
			break;

	}
	// call the controller's action
	$controller->{$action}();
}

// Map of all controllers and functions
$controllers = 	array(
					'pages'			=> ['home', 'error'],
					'majors'		=> ['show', 'add', 'delete', 'update'],
					'professors'	=> ['show', 'add', 'delete', 'update'],
					'groups'		=> ['show', 'add', 'delete', 'update'],
					'courses'		=> ['show', 'add', 'delete', 'update'],
					'classrooms'	=> ['show', 'add', 'delete', 'update'],
					'constraints'	=> ['show', 'add', 'delete', 'update'],
					'settings'		=> ['show'],
					'classes'		=> ['show', 'add', 'delete', 'update'],
					'schedule'		=> ['show']
				);
// Trigger specific type of controller
if(array_key_exists($controller, $controllers)) {
	if(in_array($action, $controllers[$controller])) {
		call($controller, $action);
	} else {
		call('pages', 'error');
	}
} else {
	call('pages', 'error');
}

?>