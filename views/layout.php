<!DOCTYPE html>
<html>
<head>
	<title>SGS</title>
	<meta charset="utf-8">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="js/chosen_v1.6.2/chosen.min.css">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	<script type="text/javascript" src="js/chosen_v1.6.2/chosen.jquery.min.js"></script>
	<script type="text/javascript" src="js/scripts.js"></script>
</head>
<body>
	<div class="logo center">
		<a href="?controller=pages&action=home"></a>
		<span class="fa-calendar"></span>
	</div>
	<h1 class="title center">Schedule Data Manager</h1>

	<?php
		// Include templates routes
		require_once('routes.php');
	?>
	<footer>
		<div class="copyright">
			<img src=""/>
			<p>@2016 Botez Costin</p>
		</div>
	</footer>
</body>
</html>