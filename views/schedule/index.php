<div class="container schedule">
	<div class="row">
		<div class="col-md-5">
			<form method="post" action="?controller=classes&action=show">
				<h2> CREATE CLASS </h2>
				<h3> 1. Choose professor </h3>
				<select data-placeholder="Choose professor" name="professor" id="professor" class="chosen-select form-control">
					<optgroup label="Facultatea de Matematica si Informatica">
					<?php
						foreach ($professors as $professor) {
							echo "<option value='$professor->id'>" . $professor->grade->name . " $professor->name</option>";
						}
					?>
					</optgroup>
				</select>
				<h3> 2. Choose group </h3>
				<select data-placeholder="Choose group" name="group" id="group" class="chosen-select form-control">
					<?php
						foreach ($groups as $group) {
							echo "<option value='$group->id'>$group->name</option>";
						}
					?>
				</select>
				<h3> 3. Choose course </h3>
				<select data-placeholder="Choose course" name="course" id="course" class="chosen-select form-control">
					<optgroup label="Facultatea de Matematica si Informatica">
					<?php
						foreach ($courses as $course) {
							echo "<option value='$course->id'>$course->name</option>";
						}
					?>
					</optgroup>
				</select>
				<h3> 4. Choose classroom </h3>
				<select data-placeholder="Choose classroom" name="classroom" id="classroom" class="chosen-select form-control">
					<?php
						foreach ($classrooms as $classroom) {
							echo "<option value='$classroom->id'>$classroom->name</option>";
						}
					?>
				</select>
				<input type="submit" class="form-control" value="CREATE"/>
			</form>
		</div>
		<div class="col-md-2"></div>
		<div class="col-md-5">
			<h2> AVAILABLE CLASSES </h2>
			<div class="classes">
				<?php foreach ($classes as $class) : ?>
					<div class="class">
						Class #1:
					</div>
				<?php endforeach; ?>
			</div>
		<?php //echo '<pre>'; print_r($contraints); echo '</pre>'; ?>
		</div>
		<?php //echo '<pre>'; print_r($majors); echo '</pre>'; ?>

		<?php //echo '<pre>'; print_r($professors); echo '</pre>'; ?>
		<?php //echo '<pre>'; print_r($courses); echo '</pre>'; ?>
		<?php //echo '<pre>'; print_r($classrooms); echo '</pre>'; ?>
		<?php //echo '<pre>'; print_r($groups); echo '</pre>'; ?>
	</div>
</div>
