<div class="container classrooms">
	<div class="row">
		<div class="panel panel-info col-md-7 classrooms-table">
			<div class="panel-heading center">CLASSROOMS TABLE</div>
		  	<table class="table table-hover">
				<thead>
					<tr>
					<?php
						$thead = get_class_vars('Classroom');
						foreach ($thead as $th => $v) {
							echo '<th>' . strtoupper($th) . '</th>';
						}
					?>
					</tr>
				</thead>
				<tbody>
					<?php
						foreach ($classrooms as $classroom) {
							echo '<tr>';
							echo 	'<td>' . $classroom->id 	. '</td>';
							echo 	'<td>' . $classroom->name 	. '</td>';
							echo 	'<td>' . $classroom->size 	. '</td>';
							echo 	'<td>' . $classroom->type 	. '</td>';
							echo '</tr>';
						}
					?>
				</tbody>
			</table>
		</div>
		<div class="col-md-1"></div>
		<div class="col-md-4">
			<div class="col-md-12">
				<div class="col-md-2"></div>
				<div class="col-md-8 center">
					<span class="fa fa-plus"></span>
					<a href="?controller=classrooms&action=add" class="add-classroom"></a>
				</div>
				<div class="col-md-2"></div>
			</div>
			<div class="col-md-12">
				<div class="col-md-2"></div>
				<div class="col-md-8 center">
					<span class="fa fa-edit"></span>
					<a href="?controller=classrooms&action=update" class="edit-classroom"></a>
				</div>
				<div class="col-md-2"></div>
			</div>
			<div class="col-md-12">
				<div class="col-md-2"></div>
				<div class="col-md-8 center">
					<span class="fa fa-trash"></span>
					<a href="?controller=classrooms&action=delete" class="delete-classroom"></a>
				</div>
				<div class="col-md-2"></div>
			</div>
			<div class="col-md-12 back">
				<div class="col-md-2"></div>
				<div class="col-md-8 center">
					<span class="fa fa-hand-o-left"></span>
					<a href="?controller=pages&action=home"></a>
				</div>
				<div class="col-md-2"></div>
			</div>
		</div>
	</div>
</div>
