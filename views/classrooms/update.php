<div class="container classrooms">
	<div class="row">
		<div class="col-md-1"></div>
		<div class="col-md-4">
			<div class="col-md-12">
				<div class="col-md-2"></div>
				<div class="col-md-8 center">
					<span class="fa fa-plus"></span>
					<a href="?controller=classrooms&action=add" class="add-classroom"></a>
				</div>
				<div class="col-md-2"></div>
			</div>
			<div class="col-md-12">
				<div class="col-md-2"></div>
				<div class="col-md-8 center">
					<span class="fa fa-edit"></span>
					<a href="?controller=classrooms&action=update" class="edit-classroom"></a>
				</div>
				<div class="col-md-2"></div>
			</div>
			<div class="col-md-12">
				<div class="col-md-2"></div>
				<div class="col-md-8 center">
					<span class="fa fa-trash"></span>
					<a href="?controller=classrooms&action=delete" class="delete-classroom"></a>
				</div>
				<div class="col-md-2"></div>
			</div>
			<div class="col-md-12 back">
				<div class="col-md-2"></div>
				<div class="col-md-8 center">
					<span class="fa fa-hand-o-left"></span>
					<a href="?controller=classrooms&action=show"></a>
				</div>
				<div class="col-md-2"></div>
			</div>
		</div>
		<div class="panel panel-info col-md-4 center edit-classroom">
			<div class="panel-heading center">EDIT CLASSROOM</div>
			<form method="post" action="?controller=classrooms&action=show">
				<select name="id" class="form-control" data-fields="input:name,input:size,select:type_id" data-ajax="classrooms">
					<option value="0">Choose classroom...</option>
					<?php
						foreach ($classrooms as $classroom) {
						 	echo "<option value='$classroom->id'>$classroom->name</option>";
						 }
					?>
				</select>
				<input type="text" name="name" class="form-control" placeholder="New name">
				<input type="text" name="size" class="form-control" placeholder="New size">
				<select name="type_id" class="form-control">
					<option value="0">New type...</option>
					<?php
						foreach ($classrooms_type as $classroom_type) {
						 	echo "<option value='$classroom_type[id]'>$classroom_type[name]</option>";
						 }
					?>
				</select>
				<input type="hidden" name="form" value="update-classroom">
				<input type="submit" name="submit" class="form-control" value="EDIT CLASSROOM"/>
			</form>
		</div>
	</div>
</div>
