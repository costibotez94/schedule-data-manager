<div class="container classrooms">
	<div class="row">
		<div class="col-md-1"></div>
		<div class="col-md-4">
			<div class="col-md-12">
				<div class="col-md-2"></div>
				<div class="col-md-8 center">
					<span class="fa fa-plus"></span>
					<a href="?controller=classrooms&action=add" class="add-classroom"></a>
				</div>
				<div class="col-md-2"></div>
			</div>
			<div class="col-md-12">
				<div class="col-md-2"></div>
				<div class="col-md-8 center">
					<span class="fa fa-edit"></span>
					<a href="?controller=classrooms&action=update" class="edit-classroom"></a>
				</div>
				<div class="col-md-2"></div>
			</div>
			<div class="col-md-12">
				<div class="col-md-2"></div>
				<div class="col-md-8 center">
					<span class="fa fa-trash"></span>
					<a href="?controller=classrooms&action=delete" class="delete-classroom"></a>
				</div>
				<div class="col-md-2"></div>
			</div>
			<div class="col-md-12 back">
				<div class="col-md-2"></div>
				<div class="col-md-8 center">
					<span class="fa fa-hand-o-left"></span>
					<a href="?controller=classrooms&action=show"></a>
				</div>
				<div class="col-md-2"></div>
			</div>
		</div>
		<div class="panel panel-info col-md-4 center add-classroom">
			<div class="panel-heading center">NEW CLASSROOM</div>
			<form method="post" action="?controller=classrooms&action=show">
				<input type="text" name="name" class="form-control" placeholder="Name">
				<input type="text" name="size" class="form-control" placeholder="Size">
				<select name="type" class="form-control">
					<option value="0">Type...</option>
					<?php
						foreach ($classrooms_type as $classroom_type) {
						 	echo "<option value='$classroom_type[id]'>$classroom_type[name]</option>";
						 }
					?>
				</select>
				<input type="hidden" name="form" value="add-classroom">
				<input type="submit" name="submit" class="form-control" value="ADD CLASSROOM"/>
			</form>
		</div>
	</div>
</div>
