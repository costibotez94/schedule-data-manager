<div class="container groups">
	<div class="row">
		<div class="col-md-1"></div>
		<div class="col-md-4">
			<div class="col-md-12">
				<div class="col-md-2"></div>
				<div class="col-md-8 center">
					<span class="fa fa-plus"></span>
					<a href="?controller=groups&action=add" class="add-group"></a>
				</div>
				<div class="col-md-2"></div>
			</div>
			<div class="col-md-12">
				<div class="col-md-2"></div>
				<div class="col-md-8 center">
					<span class="fa fa-edit"></span>
					<a href="?controller=groups&action=update" class="edit-group"></a>
				</div>
				<div class="col-md-2"></div>
			</div>
			<div class="col-md-12">
				<div class="col-md-2"></div>
				<div class="col-md-8 center">
					<span class="fa fa-trash"></span>
					<a href="?controller=groups&action=delete" class="delete-group"></a>
				</div>
				<div class="col-md-2"></div>
			</div>
			<div class="col-md-12 back">
				<div class="col-md-2"></div>
				<div class="col-md-8 center">
					<span class="fa fa-hand-o-left"></span>
					<a href="?controller=groups&action=show"></a>
				</div>
				<div class="col-md-2"></div>
			</div>
		</div>
		<div class="panel panel-warning col-md-4 center add-group">
			<div class="panel-heading center">NEW GROUP</div>
			<form method="post" action="?controller=groups&action=show">
				<input type="text" name="name" class="form-control" placeholder="Name">
				<input type="text" name="size" class="form-control" placeholder="Size">
				<input type="hidden" name="major_id" class="form-control" value="<?php echo $major_id; ?>">
				<input type="text" name="major_name" class="form-control" value="<?php echo $major_name; ?>" readonly>
				<select name="series_id" id="series_id" class="form-control">
					<option value="0">Choose serie...</option>
					<?php
						foreach ($series as $serie) {
							echo '<option value="' . $serie[0] . '">Anul ' . $serie[1] .'-'. $serie[2] . '</option>';
						}
					?>
				</select>
				<input type="hidden" name="form" value="add-group">
				<input type="submit" name="submit" class="form-control" value="ADD GROUP"/>
			</form>
		</div>
	</div>
</div>