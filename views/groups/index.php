<div class="container groups">
	<div class="row">
		<div class="panel panel-warning col-md-7 groups-table">
			<div class="panel-heading center">GROUPS TABLE</div>
		  	<table class="table table-hover">
				<thead>
					<tr>
					<?php
						$thead = get_class_vars('Group');
						foreach ($thead as $th => $v) {
							if($th != "id")
								echo '<th>' . strtoupper($th) . '</th>';
							else
								echo '<th></th>';
						}
					?>
					</tr>
				</thead>
				<tbody>
					<?php
						foreach ($groups as $group) {
							echo '<tr>';
							echo 	'<td></td>';
							echo 	'<td>' . $group->name 		. '</td>';
							echo 	'<td>' . $group->size 		. '</td>';
							echo 	'<td>' . $group->series_name. '</td>';
							echo '</tr>';
						}
					?>
				</tbody>
			</table>
		</div>
		<div class="col-md-1"></div>
		<div class="col-md-4">
			<div class="col-md-12">
				<div class="col-md-2"></div>
				<div class="col-md-8 center">
					<span class="fa fa-plus"></span>
					<a href="?controller=groups&action=add" class="add-group"></a>
				</div>
				<div class="col-md-2"></div>
			</div>
			<div class="col-md-12">
				<div class="col-md-2"></div>
				<div class="col-md-8 center">
					<span class="fa fa-edit"></span>
					<a href="?controller=groups&action=update" class="edit-group"></a>
				</div>
				<div class="col-md-2"></div>
			</div>
			<div class="col-md-12">
				<div class="col-md-2"></div>
				<div class="col-md-8 center">
					<span class="fa fa-trash"></span>
					<a href="?controller=groups&action=delete" class="delete-group"></a>
				</div>
				<div class="col-md-2"></div>
			</div>
			<div class="col-md-12 back">
				<div class="col-md-2"></div>
				<div class="col-md-8 center">
					<span class="fa fa-hand-o-left"></span>
					<a href="?controller=pages&action=home"></a>
				</div>
				<div class="col-md-2"></div>
			</div>
		</div>
	</div>
</div>
