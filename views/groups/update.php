<div class="container groups">
	<div class="row">
		<div class="col-md-1"></div>
		<div class="col-md-4">
			<div class="col-md-12">
				<div class="col-md-2"></div>
				<div class="col-md-8 center">
					<span class="fa fa-plus"></span>
					<a href="?controller=groups&action=add" class="add-group"></a>
				</div>
				<div class="col-md-2"></div>
			</div>
			<div class="col-md-12">
				<div class="col-md-2"></div>
				<div class="col-md-8 center">
					<span class="fa fa-edit"></span>
					<a href="?controller=groups&action=update" class="edit-group"></a>
				</div>
				<div class="col-md-2"></div>
			</div>
			<div class="col-md-12">
				<div class="col-md-2"></div>
				<div class="col-md-8 center">
					<span class="fa fa-trash"></span>
					<a href="?controller=groups&action=delete" class="delete-group"></a>
				</div>
				<div class="col-md-2"></div>
			</div>
			<div class="col-md-12 back">
				<div class="col-md-2"></div>
				<div class="col-md-8 center">
					<span class="fa fa-hand-o-left"></span>
					<a href="?controller=groups&action=show"></a>
				</div>
				<div class="col-md-2"></div>
			</div>
		</div>
		<div class="panel panel-warning col-md-4 center edit-group">
			<div class="panel-heading center">EDIT GROUP</div>
			<form method="post" action="?controller=groups&action=show">
				<select name="id" class="form-control" data-fields="input:size" data-ajax="groups">
					<option value="0">Choose group...</option>
					<?php
						foreach ($groups as $group) {
						 	echo "<option value='$group[id]'>$group[major_name]-$group[serie_name]-$group[name]</option>";
						 }
					?>
				</select>
				<input type="text" name="size" class="form-control" placeholder="New size">
				<input type="hidden" name="form" value="update-group">
				<input type="submit" name="submit" class="form-control" value="EDIT GROUP"/>
			</form>
		</div>
	</div>
</div>