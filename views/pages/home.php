<div class="container home">
	<div class="row">
		<div class="col-md-8">
			<div class="col-md-5 center faculty">
				<span class="fa fa-university"></span>
				<a href="?controller=majors&action=show"></a>
				<label>MAJORS</label>
			</div>
			<div class="col-md-1"></div>
			<div class="col-md-5 center professor">
				<span class="fa fa-mortar-board"></span>
				<a href="?controller=professors&action=show"></a>
				<label>PROFESSORS</label>
			</div>
			<div class="clear"></div>
			<div class="col-md-5 center courses">
				<span class="fa fa-book"></span>
				<a href="?controller=courses&action=show"></a>
				<label>COURSES</label>
			</div>
			<div class="col-md-1"></div>
			<div class="col-md-5 center groups">
				<span class="fa fa-users"></span>
				<a href="?controller=groups&action=show"></a>
				<label>GROUPS</label>
			</div>
			<div class="clear"></div>
			<div class="col-md-5 center classrooms">
				<span class="fa fa-hotel"></span>
				<a href="?controller=classrooms&action=show"></a>
				<label>CLASSROOMS</label>
			</div>
			<div class="col-md-1"></div>
			<div class="col-md-5 center constraints">
				<span class="fa fa-key"></span>
				<a href="?controller=constraints&action=show"></a>
				<label>CONSTRAINTS</label>
			</div>
			<div class="clear"></div>
			<div class="col-md-11 center classes">
				<span class="fa fa-bar-chart"></span>
				<a href="?controller=classes&action=show"></a>
				<label>CLASSES</label>
			</div>
			<!-- <div class="col-md-1"></div>
			<div class="col-md-5 center">
				<span class="fa fa-bar-chart"></span>
				<a href="?controller=schedule&action=show"></a>
				<label>CREATE SCHEDULE</label>
			</div> -->
			<div class="clear"></div>
		</div>
		<div class="col-md-4">
			<!-- <div class="col-md-12">
				<div class="col-md-2"></div>
				<div class="col-md-8 center">
					<span class="fa fa-archive"></span>
					<a href=""></a>
					<label>ARCHIVES</label>
				</div>
				<div class="col-md-2"></div>
			</div> -->
			<div class="col-md-12">
				<div class="col-md-2"></div>
				<div class="col-md-8 center settings">
					<span class="fa fa-gears"></span>
					<a href="?controller=settings&action=show"></a>
					<label>SETTINGS</label>
				</div>
				<div class="col-md-2"></div>
			</div>
			<!-- <div class="col-md-12">
				<div class="col-md-2"></div>
				<div class="col-md-8 center">
					<span class="fa fa-question"></span>
					<a href=""></a>
					<label>HELP</label>
				</div>
				<div class="col-md-2"></div>
			</div> -->
			<div class="col-md-12">
				<div class="col-md-2"></div>
				<div class="col-md-8 center info">
					<span class="fa fa-info"></span>
					<a href="" data-toggle="modal" data-target="#info-schedule"></a>
					<label>ABOUT</label>
				</div>
				<div class="col-md-2"></div>
			</div>
			<!-- Modal -->
			<div class="modal fade" id="info-schedule" tabindex="-1" role="dialog" aria-labelledby="schedule-label">
				<div class="modal-dialog" role="document">
			    	<div class="modal-content">
			      		<div class="modal-header">
			        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			        		<h4 class="modal-title" id="schedule-label">About</h4>
			      		</div>
				      	<div class="modal-body">
				        	<div>
								<h4>English</h4>
								<p>The web-app is a management system of academic schedules and is composed by 8 modules, 8 controllers, 38 views and a general settings panel that works by MVC pattern. Each controller communicates with his own module and view independently according to the user's action on the system.
								</p>
								<h4>Română</h4>
								<p>Aplicația reprezintă un sistem de gestiune a orarelor în mediul universitar ce are în componența 8 module, 8 controlleri, 38 view-uri și panoul de setări generale ce funcționează dupa pattern-ul MVC. Fiecare controller comunică cu modulul și view-urile aferente în mod independent în funcție de acțiunea utilizatorului asupra sistemului.</p>
								<hr>
								<p>@2016 Botez Costin - Dragos</p>
								<p>Web Developer at Akson</p>
				        	</div>
				      	</div>
				      	<div class="modal-footer">
				        	<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				      	</div>
			    	</div>
			  	</div>
			</div>
		</div>
	</div>
</div>