<div class="container courses">
	<div class="row">
		<div class="col-md-1"></div>
		<div class="col-md-4">
			<div class="col-md-12">
				<div class="col-md-2"></div>
				<div class="col-md-8 center">
					<span class="fa fa-plus"></span>
					<a href="?controller=courses&action=add" class="add-course"></a>
				</div>
				<div class="col-md-2"></div>
			</div>
			<div class="col-md-12">
				<div class="col-md-2"></div>
				<div class="col-md-8 center">
					<span class="fa fa-edit"></span>
					<a href="?controller=courses&action=update" class="edit-course"></a>
				</div>
				<div class="col-md-2"></div>
			</div>
			<div class="col-md-12">
				<div class="col-md-2"></div>
				<div class="col-md-8 center">
					<span class="fa fa-trash"></span>
					<a href="?controller=courses&action=delete" class="delete-course"></a>
				</div>
				<div class="col-md-2"></div>
			</div>
			<div class="col-md-12 back">
				<div class="col-md-2"></div>
				<div class="col-md-8 center">
					<span class="fa fa-hand-o-left"></span>
					<a href="?controller=courses&action=show"></a>
				</div>
				<div class="col-md-2"></div>
			</div>
		</div>
		<div class="panel panel-primary col-md-4 add-course">
			<div class="panel-heading center">NEW COURSE</div>
			<form method="post" action="?controller=courses&action=show">
				<input type="text" name="name" class="form-control" placeholder="Name">
				<select name="evaluation" id="evaluation" class="form-control chosen-select">
					<option value="0">Choose evaluation...</option>
					<option value="e">Examen</option>
					<option value="c">Colocviu</option>
				</select>
				<select name="type" id="type" class="form-control chosen-select">
					<option value="0">Choose type...</option>
					<option value="obligatoriu">Obligatoriu</option>
					<option value="facultativ">Facultativ</option>
				</select>
				<select name="semester" id="semester" class="form-control chosen-select">
					<option value="0">Choose semester...</option>
					<option value="1">1</option>
					<option value="2">2</option>
				</select>
				<select name="year" id="year" class="form-control chosen-select">
					<option value="0">Choose year...</option>
					<option value="1">1</option>
					<option value="2">2</option>
					<option value="3">3</option>
					<option value="4">4</option>
					<option value="5">5</option>
					<option value="6">6</option>
				</select>
				<select name="major_id" id="major_id" class="form-control chosen-select">
					<option value="0">Choose major...</option>
					<?php
						foreach ($majors as $major) {
							echo "<option value='$major->id'>$major->name</option>";
						}
					?>
				</select>
				<input type="text" name="credits" class="form-control" placeholder="Credits">
				<?php //echo '<pre>'; print_r($professors); echo '</pre>'; ?>
				<select data-placeholder="Choose professors" name="professors[]" id="professors" class="chosen-select form-control" multiple>
					<optgroup label="Facultatea de Matematica si Informatica">
					<?php
						foreach ($professors as $professor) {
							echo "<option value='$professor->id'>" . $professor->grade->name . " $professor->name</option>";
						}
					?>
					</optgroup>
				</select>
				<input type="hidden" name="form" value="add-course">
				<input type="submit" name="submit" class="form-control" value="ADD COURSE"/>
			</form>
		</div>
	</div>
</div>
