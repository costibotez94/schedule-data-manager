<div class="container courses">
	<div class="row">
		<div class="col-md-1"></div>
		<div class="col-md-4">
			<div class="col-md-12">
				<div class="col-md-2"></div>
				<div class="col-md-8 center">
					<span class="fa fa-plus"></span>
					<a href="?controller=courses&action=add" class="add-course"></a>
				</div>
				<div class="col-md-2"></div>
			</div>
			<div class="col-md-12">
				<div class="col-md-2"></div>
				<div class="col-md-8 center">
					<span class="fa fa-edit"></span>
					<a href="?controller=courses&action=update" class="edit-course"></a>
				</div>
				<div class="col-md-2"></div>
			</div>
			<div class="col-md-12">
				<div class="col-md-2"></div>
				<div class="col-md-8 center">
					<span class="fa fa-trash"></span>
					<a href="?controller=courses&action=delete" class="delete-course"></a>
				</div>
				<div class="col-md-2"></div>
			</div>
			<div class="col-md-12 back">
				<div class="col-md-2"></div>
				<div class="col-md-8 center">
					<span class="fa fa-hand-o-left"></span>
					<a href="?controller=courses&action=show"></a>
				</div>
				<div class="col-md-2"></div>
			</div>
		</div>
		<div class="panel panel-primary col-md-4 center delete-course">
			<div class="panel-heading center">DELETE COURSE</div>
			<form method="post" action="?controller=courses&action=show">
				<select name="id" name="id" class="form-control">
					<option value="0">Choose course...</option>
					<?php
						foreach ($courses as $course) {
						 	echo "<option value='$course->id'>$course->name</option>";
						 }
					?>
				</select>
				<input type="hidden" name="form" value="delete-course">
				<input type="submit" name="submit" class="form-control" value="DELETE COURSE"/>
			</form>
		</div>
	</div>
</div>
