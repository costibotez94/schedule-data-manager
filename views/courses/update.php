<div class="container courses">
	<div class="row">
		<div class="col-md-1"></div>
		<div class="col-md-4">
			<div class="col-md-12">
				<div class="col-md-2"></div>
				<div class="col-md-8 center">
					<span class="fa fa-plus"></span>
					<a href="?controller=courses&action=add" class="add-course"></a>
				</div>
				<div class="col-md-2"></div>
			</div>
			<div class="col-md-12">
				<div class="col-md-2"></div>
				<div class="col-md-8 center">
					<span class="fa fa-edit"></span>
					<a href="?controller=courses&action=update" class="edit-course"></a>
				</div>
				<div class="col-md-2"></div>
			</div>
			<div class="col-md-12">
				<div class="col-md-2"></div>
				<div class="col-md-8 center">
					<span class="fa fa-trash"></span>
					<a href="?controller=courses&action=delete" class="delete-course"></a>
				</div>
				<div class="col-md-2"></div>
			</div>
			<div class="col-md-12 back">
				<div class="col-md-2"></div>
				<div class="col-md-8 center">
					<span class="fa fa-hand-o-left"></span>
					<a href="?controller=courses&action=show"></a>
				</div>
				<div class="col-md-2"></div>
			</div>
		</div>
		<div class="panel panel-primary col-md-4 center edit-course">
			<div class="panel-heading center">EDIT COURSE</div>
			<form method="post" action="?controller=courses&action=show">
				<select name="id" name="id" class="form-control" data-fields="select:evaluation,select:type,select:semester,select:year,select:major_id,input:credits" data-ajax="courses">
					<option value="0">Choose course...</option>
					<?php
						foreach ($courses as $course) {
						 	echo "<option value='$course->id'>$course->name</option>";
						 }
					?>
				</select>
				<select name="evaluation" id="evaluation" class="form-control">
					<option value="0">Choose evaluation...</option>
					<option value="e">Examen</option>
					<option value="c">Colocviu</option>
				</select>
				<select name="type" id="type" class="form-control">
					<option value="0">Choose type...</option>
					<option value="obligatoriu">Obligatoriu</option>
					<option value="facultativ">Facultativ</option>
				</select>
				<select name="semester" id="semester" class="form-control">
					<option value="0">Choose semester...</option>
					<option value="1">Semestru 1</option>
					<option value="2">Semestru 2</option>
				</select>
				<select name="year" id="year" class="form-control">
					<option value="0">Choose year...</option>
					<option value="1">Anul 1</option>
					<option value="2">Anul 2</option>
					<option value="3">Anul 3</option>
					<option value="4">Anul 4</option>
					<option value="5">Anul 5</option>
					<option value="6">Anul 6</option>
				</select>
				<select name="major_id" id="major_id" class="form-control">
					<option value="0">Choose new major...</option>
					<?php
						foreach ($majors as $major) {
							echo "<option value='$major->id'>$major->name</option>";
						}
					?>
				</select>
				<input type="text" name="credits" class="form-control" placeholder="New credits">
				<input type="hidden" name="form" value="update-course">
				<input type="submit" name="submit" class="form-control" value="EDIT COURSE"/>
			</form>
		</div>
	</div>
</div>
