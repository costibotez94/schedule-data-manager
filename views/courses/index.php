<div class="container courses">
	<div class="row">
		<div class="panel panel-primary col-md-7 courses-table">
			<div class="panel-heading center">COURSES TABLE</div>
		  	<table class="table table-hover">
				<thead>
					<tr>
					<?php
						$thead = get_class_vars('Course');
						foreach ($thead as $th => $v)
							if(strtoupper($th) !='ID')
								echo '<th>' . str_replace('_', ' ', strtoupper($th)) . '</th>';
					?>
					</tr>
				</thead>
				<tbody>
					<?php
						foreach ($courses as $course) {
							echo '<tr>';
							echo 	'<td>' . $course->name 	. '</td>';
							echo 	'<td>' . ($course->evaluation == 'c' ? 'Colocviu' : 'Examen') 	. '</td>';
							echo 	'<td>' . ucfirst($course->type) 	. '</td>';
							echo 	'<td>' . $course->semester 	. '</td>';
							echo 	'<td>' . $course->year 	. '</td>';
							echo 	'<td>' . $course->major_name 	. '</td>';
							echo 	'<td>' . $course->credits 	. '</td>';
							echo '</tr>';
						}
					?>
				</tbody>
			</table>
		</div>
		<div class="col-md-1"></div>
		<div class="col-md-4">
			<div class="col-md-12">
				<div class="col-md-2"></div>
				<div class="col-md-8 center">
					<span class="fa fa-plus"></span>
					<a href="?controller=courses&action=add" class="add-course"></a>
				</div>
				<div class="col-md-2"></div>
			</div>
			<div class="col-md-12">
				<div class="col-md-2"></div>
				<div class="col-md-8 center">
					<span class="fa fa-edit"></span>
					<a href="?controller=courses&action=update" class="edit-course"></a>
				</div>
				<div class="col-md-2"></div>
			</div>
			<div class="col-md-12">
				<div class="col-md-2"></div>
				<div class="col-md-8 center">
					<span class="fa fa-trash"></span>
					<a href="?controller=courses&action=delete" class="delete-course"></a>
				</div>
				<div class="col-md-2"></div>
			</div>
			<div class="col-md-12 back">
				<div class="col-md-2"></div>
				<div class="col-md-8 center">
					<span class="fa fa-hand-o-left"></span>
					<a href="?controller=pages&action=home"></a>
				</div>
				<div class="col-md-2"></div>
			</div>
		</div>
	</div>
</div>
