<div class="container professors">
	<div class="row">
		<div class="col-md-1"></div>
		<div class="col-md-4">
			<div class="col-md-12">
				<div class="col-md-2"></div>
				<div class="col-md-8 center">
					<span class="fa fa-plus"></span>
					<a href="?controller=professors&action=add" class="add-professor"></a>
				</div>
				<div class="col-md-2"></div>
			</div>
			<div class="col-md-12">
				<div class="col-md-2"></div>
				<div class="col-md-8 center">
					<span class="fa fa-edit"></span>
					<a href="?controller=professors&action=update" class="edit-professor"></a>
				</div>
				<div class="col-md-2"></div>
			</div>
			<div class="col-md-12">
				<div class="col-md-2"></div>
				<div class="col-md-8 center">
					<span class="fa fa-trash"></span>
					<a href="?controller=professors&action=delete" class="delete-professor"></a>
				</div>
				<div class="col-md-2"></div>
			</div>
			<div class="col-md-12 back">
				<div class="col-md-2"></div>
				<div class="col-md-8 center">
					<span class="fa fa-hand-o-left"></span>
					<a href="?controller=professors&action=show"></a>
				</div>
				<div class="col-md-2"></div>
			</div>
		</div>
		<div class="panel panel-success col-md-4 center add-professor">
			<div class="panel-heading center">NEW PROFESSOR</div>
			<form method="post" action="?controller=professors&action=show">
				<input type="text" name="name" class="form-control" placeholder="Name">
				<?php //echo '<pre>'; print_r($grades); echo '</pre>'; ?>
				<select name="grade" id="grade" class="form-control">
					<option value="0">Choose grade...</option>
					<?php
						foreach ($grades as $grade) {
							echo "<option value='$grade->id'>$grade->name</option>";
						}
					?>
				</select>
				<input type="hidden" name="form" value="add-professor">
				<input type="submit" name="submit" class="form-control" value="ADD PROFESSOR"/>
			</form>
		</div>
	</div>
</div>
