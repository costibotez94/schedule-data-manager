<div class="container professors">
	<div class="row">
		<div class="col-md-1"></div>
		<div class="col-md-4">
			<div class="col-md-12">
				<div class="col-md-2"></div>
				<div class="col-md-8 center">
					<span class="fa fa-plus"></span>
					<a href="?controller=professors&action=add" class="add-professor"></a>
				</div>
				<div class="col-md-2"></div>
			</div>
			<div class="col-md-12">
				<div class="col-md-2"></div>
				<div class="col-md-8 center">
					<span class="fa fa-edit"></span>
					<a href="?controller=professors&action=update" class="edit-professor"></a>
				</div>
				<div class="col-md-2"></div>
			</div>
			<div class="col-md-12">
				<div class="col-md-2"></div>
				<div class="col-md-8 center">
					<span class="fa fa-trash"></span>
					<a href="?controller=professors&action=delete" class="delete-professor"></a>
				</div>
				<div class="col-md-2"></div>
			</div>
			<div class="col-md-12 back">
				<div class="col-md-2"></div>
				<div class="col-md-8 center">
					<span class="fa fa-hand-o-left"></span>
					<a href="?controller=professors&action=show"></a>
				</div>
				<div class="col-md-2"></div>
			</div>
		</div>
		<div class="panel panel-success col-md-4 center edit-professor">
			<div class="panel-heading center">EDIT PROFESSOR</div>
			<form method="post" action="?controller=professors&action=show">
				<select name="id" class="form-control" data-fields="input:name,select:grade_id" data-ajax="professors">
					<option value="0">Choose professor...</option>
					<?php
						foreach ($professors as $professor) {
						 	echo "<option value='$professor->id'>$professor->name</option>";
						 }
					?>
				</select>
				<input type="text" name="name" class="form-control" placeholder="New name">
				<select name="grade_id" class="form-control">
					<option value="0">New grade...</option>
					<?php
						foreach ($grades as $grade) {
						 	echo "<option value='$grade->id'>$grade->name</option>";
						 }
					?>
				</select>
				<!-- <input type="text" name="grade_id" class="form-control" placeholder="New grade"> -->
				<input type="hidden" name="form" value="update-professor">
				<input type="submit" name="submit" class="form-control" value="EDIT PROFESSOR"/>
			</form>
		</div>
	</div>
</div>
