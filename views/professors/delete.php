<div class="container professors">
	<div class="row">
		<div class="col-md-1"></div>
		<div class="col-md-4">
			<div class="col-md-12">
				<div class="col-md-2"></div>
				<div class="col-md-8 center">
					<span class="fa fa-plus"></span>
					<a href="?controller=professors&action=add" class="add-professor"></a>
				</div>
				<div class="col-md-2"></div>
			</div>
			<div class="col-md-12">
				<div class="col-md-2"></div>
				<div class="col-md-8 center">
					<span class="fa fa-edit"></span>
					<a href="?controller=professors&action=update" class="edit-professor"></a>
				</div>
				<div class="col-md-2"></div>
			</div>
			<div class="col-md-12">
				<div class="col-md-2"></div>
				<div class="col-md-8 center">
					<span class="fa fa-trash"></span>
					<a href="?controller=professors&action=delete" class="delete-professor"></a>
				</div>
				<div class="col-md-2"></div>
			</div>
			<div class="col-md-12 back">
				<div class="col-md-2"></div>
				<div class="col-md-8 center">
					<span class="fa fa-hand-o-left"></span>
					<a href="?controller=professors&action=show"></a>
				</div>
				<div class="col-md-2"></div>
			</div>
		</div>
		<div class="panel panel-success col-md-4 center delete-professor">
			<div class="panel-heading center">DELETE PROFESSOR</div>
			<form method="post" action="?controller=professors&action=show">
				<select name="id" class="form-control">
					<option value="0">Choose professor...</option>
					<?php
						foreach ($professors as $professor) {
							echo "<option value='$professor->id'>$professor->name</option>";
						}
					?>
				</select>
				<input type="hidden" name="form" value="delete-professor">
				<input type="submit" name="submit" class="form-control" value="DELETE PROFESSOR"/>
			</form>
		</div>
	</div>
</div>
