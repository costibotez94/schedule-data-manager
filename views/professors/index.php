<div class="container professors">
	<div class="row">
		<div class="panel panel-success col-md-7 professors-table">
			<div class="panel-heading center">PROFESSORS TABLE</div>
		  	<table class="table table-hover">
				<thead>
					<tr>
					<?php
						$thead = get_class_vars('Professor');
						foreach ($thead as $th => $v) {
							if(strtoupper($th) !='ID')
								echo '<th>' . strtoupper($th) . '</th>';
						}
					?>
					</tr>
				</thead>
				<tbody>
					<?php
						foreach ($professors as $professor) {
							echo '<tr>';
							// echo 	'<td>' . $professor->id 	. '</td>';
							echo 	'<td>' . $professor->grade->name 	. '</td>';
							echo 	'<td>' . $professor->name 	. '</td>';
							echo '</tr>';
						}
					?>
				</tbody>
			</table>
		</div>
		<div class="col-md-1"></div>
		<div class="col-md-4">
			<div class="col-md-12">
				<div class="col-md-2"></div>
				<div class="col-md-8 center">
					<span class="fa fa-plus"></span>
					<a href="?controller=professors&action=add" class="add-professor"></a>
				</div>
				<div class="col-md-2"></div>
			</div>
			<div class="col-md-12">
				<div class="col-md-2"></div>
				<div class="col-md-8 center">
					<span class="fa fa-edit"></span>
					<a href="?controller=professors&action=update" class="edit-professor"></a>
				</div>
				<div class="col-md-2"></div>
			</div>
			<div class="col-md-12">
				<div class="col-md-2"></div>
				<div class="col-md-8 center">
					<span class="fa fa-trash"></span>
					<a href="?controller=professors&action=delete" class="delete-professor"></a>
				</div>
				<div class="col-md-2"></div>
			</div>
			<div class="col-md-12 back">
				<div class="col-md-2"></div>
				<div class="col-md-8 center">
					<span class="fa fa-hand-o-left"></span>
					<a href="?controller=pages&action=home"></a>
				</div>
				<div class="col-md-2"></div>
			</div>
		</div>
	</div>
</div>
