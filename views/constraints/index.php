<div class="container constraints">
	<div class="row">
		<div class="panel panel-danger col-md-7 constraints-table">
			<div class="panel-heading center">CONSTRAINTS</div>
		  	<div class="contraints-form">
				<table class="table table-hover">
				<?php //echo '<pre>'; print_r($_POST); echo '</pre>'; ?>
		  		<?php //echo '<pre>'; print_r($professors); echo '</pre>'; ?>
		  		<?php //echo '<pre>'; print_r($groups); echo '</pre>'; ?>
		  		<?php //echo '<pre>'; print_r($majors); echo '</pre>'; ?>
		  		<?php //echo '<pre>'; print_r($courses); echo '</pre>'; ?>
		  		<?php //echo '<pre>'; print_r($constraints); echo '</pre>'; ?>
					<tbody>
						<?php $i=1; ?>
						<?php foreach ($constraints as $constraint_object) : ?>
						<?php 	foreach ($constraint_object->constraints as $constraint) : ?>
							<tr>
								<td><?php echo $i;?></td>
							<?php 	$i++; ?>
							<?php $j=0; ?>
							<?php 	foreach ($constraint as $constraint_pair) : ?>

							<?php 	$j++; ?>
									<!-- CONSTRAINT SELECT -->
							  		<td>
							  			<select class="form-control" name="choose_constraint">
								  			<option value="0">Choose constraint</option>
								  			<?php foreach ($available_constraints as $available_constraint) {
								  					if($constraint_pair[0] == $available_constraint)
								  						echo '<option value="' . $available_constraint . '" selected>' . $available_constraint . '</option>';
								  					else
								  						echo '<option value="' . $available_constraint . '">' . $available_constraint . '</option>';
								  			}?>
								  		</select>
							  		</td>
							<?php 	if($constraint_pair[0] == 'Professor') : ?>
										<!-- PROFESSOR SELECT -->
								  		<td>
								  			<select class="form-control" name="professor">
									  			<option value="0">Choose professor</option>
									  			<?php foreach ($professors as $professor) {
									  				if($constraint_pair[1] == $professor->id)
									  					echo '<option value="' . $professor->id . '" selected>' . $professor->grade->name . ' ' . $professor->name . '</option>';
									  				else
									  					echo '<option value="' . $professor->id . '">' . $professor->grade->name . ' ' . $professor->name . '</option>';
									  			}?>
									  		</select>
									  	</td>
							<?php 	endif; ?>
							<?php 	if($constraint_pair[0] == 'Classroom') : ?>
										<!-- CLASSROOM SELECT -->
								  		<td>
								  			<select class="form-control" name="classroom">
									  			<option value="0">Choose classroom</option>
									  			<?php foreach ($classrooms as $classroom) {
									  				if($constraint_pair[1] == $classroom->id)
									  					echo '<option value="' . $classroom->id . '" selected>' . $classroom->name . '</option>';
									  				else
									  					echo '<option value="' . $classroom->id . '">' . $classroom->name . '</option>';
									  			}?>
									  		</select>
									  	</td>
						  	<?php 	endif; ?>
							<?php 	if($constraint_pair[0] == 'Group') : ?>
										<!-- GROUP SELECT -->
										<td>
											<select class="form-control" name="group">
									  			<option value="0">Choose group</option>
									  			<?php foreach ($groups as $group) {
									  				if($constraint_pair[1] == $group->id)
									  					echo '<option value="' . $group->id . '" selected>' . $group->name . '</option>';
									  				else
									  					echo '<option value="' . $group->id . '">' . $group->name . '</option>';

									  			}?>
									  		</select>
									  	</td>
				  			<?php 	endif; ?>
							<?php 	if($constraint_pair[0] == 'Course') : ?>
										<!-- COURSE SELECT -->
								  		<td>
								  			<select class="form-control" name="course">
									  			<option value="0">Choose course</option>
									  			<?php foreach ($courses as $course) {
									  				if($constraint_pair[1] == $course->id)
									  					echo '<option value="' . $course->id . '" selected>' . $course->name . '</option>';
									  				else
									  					echo '<option value="' . $course->id . '">' . $course->name . '</option>';

									  			}?>
									  		</select>
									  	</td>
							<?php 	endif; ?>
							<?php 	if($constraint_pair[0] == 'Major') : ?>
										<!-- MAJOR SELECT -->
								  		<td>
								  			<select class="form-control" name="major">
									  			<option value="0">Choose major</option>
									  			<?php foreach ($majors as $major) {
									  				if($constraint_pair[1] == $major->id)
									  					echo '<option value="' . $major->id . '" selected>' . $major->name . '</option>';
									  				else
									  					echo '<option value="' . $major->id . '">' . $major->name . '</option>';

									  			}?>
									  		</select>
									  	</td>
					  		<?php 	endif; ?>
							<?php 	if($constraint_pair[0] == 'from_date') : ?>
							<?php 	endif; ?>
							<?php 	if($constraint_pair[0] == 'to_date') : ?>
							<?php 	endif; ?>
							<?php 	if($constraint_pair[0] == 'from_hour') : ?>
							<?php 	endif; ?>
							<?php 	if($constraint_pair[0] == 'to_hour') : ?>
							<?php 	endif; ?>
							<?php 	if($constraint_pair[0] == 'weekdays') : ?>
							<?php 	endif; ?>
							<?php 	if($j==1) : ?>
										<!-- ACTION SELECT -->
								  		<td>
								  			<select class="form-control" name="choose_action">
									  			<option value="0">Choose action</option>
									  			<?php foreach ($available_actions as $available_action) {
									  				if($constraint_object->constraint_id == $available_action[1])
									  					echo '<option value="' . $available_action[0] . '" selected>' . $available_action[1] . '</option>';
									  				else
									  					echo '<option value="' . $available_action[0] . '">' . $available_action[1] . '</option>';

									  			}?>
									  		</select>
									  	</td>
							<?php 	endif; ?>
							<?php //echo '<pre>'; print_r($constraints); echo '</pre>'; ?>
							<?php 	endforeach; ?>
						<?php 	endforeach; ?>
						</tr>
						<?php endforeach; ?>
					</tbody>
				</table>
			</div>
		</div>
		<div class="col-md-1"></div>
		<div class="col-md-4">
			<div class="col-md-12">
				<div class="col-md-2"></div>
				<div class="col-md-8 center">
					<span class="fa fa-plus"></span>
					<a href="?controller=constraints&action=add" class="add-constraints"></a>
				</div>
				<div class="col-md-2"></div>
			</div>
			<div class="col-md-12">
				<div class="col-md-2"></div>
				<div class="col-md-8 center">
					<span class="fa fa-edit"></span>
					<a href="?controller=constraints&action=update" class="edit-constraints"></a>
				</div>
				<div class="col-md-2"></div>
			</div>
			<div class="col-md-12">
				<div class="col-md-2"></div>
				<div class="col-md-8 center">
					<span class="fa fa-trash"></span>
					<a href="?controller=constraints&action=delete" class="delete-constraints"></a>
				</div>
				<div class="col-md-2"></div>
			</div>
			<div class="col-md-12 back">
				<div class="col-md-2"></div>
				<div class="col-md-8 center">
					<span class="fa fa-hand-o-left"></span>
					<a href="?controller=pages&action=home"></a>
				</div>
				<div class="col-md-2"></div>
			</div>
		</div>
	</div>
</div>
