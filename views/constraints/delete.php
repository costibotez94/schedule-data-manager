<div class="container constraints">
	<div class="row">
		<div class="col-md-1"></div>
		<div class="col-md-4">
			<div class="col-md-12">
				<div class="col-md-2"></div>
				<div class="col-md-8 center">
					<span class="fa fa-plus"></span>
					<a href="?controller=constraints&action=add" class="add-constraint"></a>
				</div>
				<div class="col-md-2"></div>
			</div>
			<div class="col-md-12">
				<div class="col-md-2"></div>
				<div class="col-md-8 center">
					<span class="fa fa-edit"></span>
					<a href="?controller=constraints&action=update" class="edit-constraint"></a>
				</div>
				<div class="col-md-2"></div>
			</div>
			<div class="col-md-12">
				<div class="col-md-2"></div>
				<div class="col-md-8 center">
					<span class="fa fa-trash"></span>
					<a href="?controller=constraints&action=delete" class="delete-constraint"></a>
				</div>
				<div class="col-md-2"></div>
			</div>
			<div class="col-md-12 back">
				<div class="col-md-2"></div>
				<div class="col-md-8 center">
					<span class="fa fa-hand-o-left"></span>
					<a href="?controller=constraints&action=show"></a>
				</div>
				<div class="col-md-2"></div>
			</div>
		</div>
		<div class="panel panel-danger col-md-4 center delete-constraint">
			<div class="panel-heading center">DELETE CONSTRAINT</div>
			<form method="post" action="?controller=constraints&action=show">
				<input type="text" name="id" class="form-control" placeholder="ID">
				<input type="hidden" name="form" value="delete-constraint">
				<input type="submit" name="submit" class="form-control" value="DELETE CONSTRAINT"/>
			</form>
		</div>
	</div>
</div>
