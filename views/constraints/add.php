<div class="container constraints">
	<div class="row">
		<div class="col-md-4">
			<div class="col-md-12">
				<div class="col-md-2"></div>
				<div class="col-md-8 center">
					<span class="fa fa-plus"></span>
					<a href="?controller=constraints&action=add" class="add-constraint"></a>
				</div>
				<div class="col-md-2"></div>
			</div>
			<div class="col-md-12">
				<div class="col-md-2"></div>
				<div class="col-md-8 center">
					<span class="fa fa-edit"></span>
					<a href="?controller=constraints&action=update" class="edit-constraint"></a>
				</div>
				<div class="col-md-2"></div>
			</div>
			<div class="col-md-12">
				<div class="col-md-2"></div>
				<div class="col-md-8 center">
					<span class="fa fa-trash"></span>
					<a href="?controller=constraints&action=delete" class="delete-constraint"></a>
				</div>
				<div class="col-md-2"></div>
			</div>
			<div class="col-md-12 back">
				<div class="col-md-2"></div>
				<div class="col-md-8 center">
					<span class="fa fa-hand-o-left"></span>
					<a href="?controller=constraints&action=show"></a>
				</div>
				<div class="col-md-2"></div>
			</div>
		</div>
		<div class="panel panel-danger col-md-8 center add-constraint">
			<div class="panel-heading center">NEW CONSTRAINT</div>
			<form method="post" action="?controller=constraints&action=show">
				<table class="table table-hover">
					<tbody>
						<tr>
							<!-- CONSTRAINT 1 SELECT -->
							<td>
					  			<select class="form-control" name="constraint_1[]">
						  			<option value="0">Choose constraint</option>
						  			<?php foreach ($available_constraints as $available_constraint) {
					  						echo '<option value="' . $available_constraint . '">' . $available_constraint . '</option>';
						  			}?>
						  		</select>
					  		</td>
					  		<td class="constraint_1">
					  			<select class="form-control" name="professor_1[]">
						  			<option value="0">Choose professor</option>
						  			<?php foreach ($professors as $professor) {
						  					echo '<option value="' . $professor->id . '">' . $professor->grade->name . ' ' . $professor->name . '</option>';
						  			}?>
						  		</select>
						  		<select class="form-control" name="classroom_1[]">
						  			<option value="0">Choose classroom</option>
						  			<?php foreach ($classrooms as $classroom) {
						  					echo '<option value="' . $classroom->id . '">' . $classroom->name . '</option>';

						  			}?>
						  		</select>
						  		<select class="form-control" name="group_1[]">
						  			<option value="0">Choose group</option>
						  			<?php foreach ($groups as $group) {
						  					echo '<option value="' . $group->id . '">' . $group->name . '</option>';

						  			}?>
						  		</select>
						  		<select class="form-control" name="course_1[]">
						  			<option value="0">Choose course</option>
						  			<?php foreach ($courses as $course) {
						  					echo '<option value="' . $course->id . '">' . $course->name . '</option>';

						  			}?>
						  		</select>
						  		<select class="form-control" name="major_1[]">
						  			<option value="0">Choose major</option>
						  			<?php foreach ($majors as $major) {
						  					echo '<option value="' . $major->id . '">' . $major->name . '</option>';

						  			}?>
						  		</select>
					  		</td>

					  		</td>
					  		<!-- ACTION SELECT -->
					  		<td>
					  			<select class="form-control" name="action[]">
						  			<option value="0">Choose action</option>
						  			<?php foreach ($available_actions as $available_action) {
						  					echo '<option value="' . $available_action[0] . '">' . $available_action[1] . '</option>';

						  			}?>
						  		</select>
						  	</td>
						  	<!-- CONSTRAINT 2 SELECT -->
							<td>
					  			<select class="form-control" name="constraint_2[]">
						  			<option value="0">Choose constraint</option>
						  			<?php foreach ($available_constraints as $available_constraint) {
					  						echo '<option value="' . $available_constraint . '">' . $available_constraint . '</option>';
						  			}?>
						  		</select>
					  		</td>
					  		<td class="constraint_2">
					  			<select class="form-control" name="professor_2[]">
						  			<option value="0">Choose professor</option>
						  			<?php foreach ($professors as $professor) {
						  					echo '<option value="' . $professor->id . '">' . $professor->grade->name . ' ' . $professor->name . '</option>';
						  			}?>
						  		</select>
						  		<select class="form-control" name="classroom_2[]">
						  			<option value="0">Choose classroom</option>
						  			<?php foreach ($classrooms as $classroom) {
						  					echo '<option value="' . $classroom->id . '">' . $classroom->name . '</option>';

						  			}?>
						  		</select>
						  		<select class="form-control" name="group_2[]">
						  			<option value="0">Choose group</option>
						  			<?php foreach ($groups as $group) {
						  					echo '<option value="' . $group->id . '">' . $group->name . '</option>';

						  			}?>
						  		</select>
						  		<select class="form-control" name="course_2[]">
						  			<option value="0">Choose course</option>
						  			<?php foreach ($courses as $course) {
						  					echo '<option value="' . $course->id . '">' . $course->name . '</option>';

						  			}?>
						  		</select>
						  		<select class="form-control" name="major_2[]">
						  			<option value="0">Choose major</option>
						  			<?php foreach ($majors as $major) {
						  					echo '<option value="' . $major->id . '">' . $major->name . '</option>';

						  			}?>
						  		</select>
					  		</td>
					  		<!-- ADD MORE -->
							<td>
					  			<button class="glyphicon glyphicon-plus-sign add-rule" aria-label="Add more"></button>
					  		</td>
						</tr>
					</tbody>
				</table>
				<input type="hidden" name="form" value="add-constraint">
				<input type="submit" name="submit" class="form-control" value="ADD CONSTRAINT"/>
			</form>
		</div>
	</div>
</div>
