<div class="container classes">
	<div class="row">
		<div class="col-md-7 classes-table">
			<?php $i=0; foreach ($classes as $class) : $i++?>
				<div class="panel panel-classes col-md-3">
					<div class="panel-heading">
						<p><?php echo "Class #$class->id"; ?></p>
					</div>
					<div class="panel-body">
						<p><?php echo $class->professor; ?></p>
						<p><?php echo $class->group; ?></p>
						<p><?php echo $class->course; ?></p>
						<p><?php echo $class->classroom; ?></p>
					</div>
				</div>
			<?php endforeach; ?>
		</div>
		<div class="col-md-1"></div>
		<div class="col-md-4">
			<div class="col-md-12">
				<div class="col-md-2"></div>
				<div class="col-md-8 center">
					<span class="fa fa-plus"></span>
					<a href="?controller=classes&action=add" class="add-classroom"></a>
				</div>
				<div class="col-md-2"></div>
			</div>
			<div class="col-md-12">
				<div class="col-md-2"></div>
				<div class="col-md-8 center">
					<span class="fa fa-edit"></span>
					<a href="?controller=classes&action=update" class="edit-classroom"></a>
				</div>
				<div class="col-md-2"></div>
			</div>
			<div class="col-md-12">
				<div class="col-md-2"></div>
				<div class="col-md-8 center">
					<span class="fa fa-trash"></span>
					<a href="?controller=classes&action=delete" class="delete-classroom"></a>
				</div>
				<div class="col-md-2"></div>
			</div>
			<div class="col-md-12 back">
				<div class="col-md-2"></div>
				<div class="col-md-8 center">
					<span class="fa fa-hand-o-left"></span>
					<a href="?controller=pages&action=home"></a>
				</div>
				<div class="col-md-2"></div>
			</div>
		</div>
	</div>
</div>
