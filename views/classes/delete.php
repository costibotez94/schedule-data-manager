<div class="container classes">
	<div class="row">
		<div class="col-md-1"></div>
		<div class="col-md-4">
			<div class="col-md-12">
				<div class="col-md-2"></div>
				<div class="col-md-8 center">
					<span class="fa fa-plus"></span>
					<a href="?controller=classes&action=add" class="add-classroom"></a>
				</div>
				<div class="col-md-2"></div>
			</div>
			<div class="col-md-12">
				<div class="col-md-2"></div>
				<div class="col-md-8 center">
					<span class="fa fa-edit"></span>
					<a href="?controller=classes&action=update" class="edit-classroom"></a>
				</div>
				<div class="col-md-2"></div>
			</div>
			<div class="col-md-12">
				<div class="col-md-2"></div>
				<div class="col-md-8 center">
					<span class="fa fa-trash"></span>
					<a href="?controller=classes&action=delete" class="delete-classroom"></a>
				</div>
				<div class="col-md-2"></div>
			</div>
			<div class="col-md-12 back">
				<div class="col-md-2"></div>
				<div class="col-md-8 center">
					<span class="fa fa-hand-o-left"></span>
					<a href="?controller=classes&action=show"></a>
				</div>
				<div class="col-md-2"></div>
			</div>
		</div>
		<div class="panel panel-classes col-md-4 center delete-classes">
			<div class="panel-heading center">DELETE CLASS</div>
			<form method="post" action="?controller=classes&action=show">
				<select name="id" class="form-control" data-fields="input:name,input:size,input:type" data-ajax="classrooms">
					<option value="0">Choose class...</option>
					<?php
						foreach ($classes as $class) {
						 	echo "<option value='$class->id'>Class #$class->id</option>";
						 }
					?>
				</select>
				<input type="hidden" name="form" value="delete-class">
				<input type="submit" name="submit" class="form-control" value="DELETE CLASS"/>
			</form>
		</div>
	</div>
</div>
