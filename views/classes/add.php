<div class="container classes">
	<div class="row">
		<div class="col-md-1"></div>
		<div class="col-md-4">
			<div class="col-md-12">
				<div class="col-md-2"></div>
				<div class="col-md-8 center">
					<span class="fa fa-plus"></span>
					<a href="?controller=classes&action=add" class="add-class"></a>
				</div>
				<div class="col-md-2"></div>
			</div>
			<div class="col-md-12">
				<div class="col-md-2"></div>
				<div class="col-md-8 center">
					<span class="fa fa-edit"></span>
					<a href="?controller=classes&action=update" class="edit-class"></a>
				</div>
				<div class="col-md-2"></div>
			</div>
			<div class="col-md-12">
				<div class="col-md-2"></div>
				<div class="col-md-8 center">
					<span class="fa fa-trash"></span>
					<a href="?controller=classes&action=delete" class="delete-class"></a>
				</div>
				<div class="col-md-2"></div>
			</div>
			<div class="col-md-12 back">
				<div class="col-md-2"></div>
				<div class="col-md-8 center">
					<span class="fa fa-hand-o-left"></span>
					<a href="?controller=classes&action=show"></a>
				</div>
				<div class="col-md-2"></div>
			</div>
		</div>
		<div class="panel panel-classes col-md-4 center add-class">
			<div class="panel-heading center">NEW CLASS</div>
			<form method="post" action="?controller=classes&action=show">
				<select name="professor" data-placeholder="Choose professor" id="professor" class="form-control chosen-select">
					<option value="0"></option>
					<optgroup label="Facultatea de Matematica si Informatica">
					<?php
						foreach ($professors as $professor) {
							echo "<option value='$professor->id'>" . $professor->grade->name . " $professor->name</option>";
						}
					?>
					</optgroup>
				</select>
				<select name="group" data-placeholder="Choose group" id="group" class="form-control chosen-select">
					<option value="0"></option>
					<?php
						foreach ($groups as $group) {
							echo "<option value='$group[id]'>$group[major_name]-$group[serie_name]-$group[name]</option>";
						}
					?>
				</select>
				<select name="course" id="course" data-placeholder="Choose course" class="form-control chosen-select">
					<option value="0"></option>
					<optgroup label="Facultatea de Matematica si Informatica">
					<?php
						foreach ($courses as $course) {
							echo "<option value='$course->id'>$course->name</option>";
						}
					?>
					</optgroup>
				</select>
				<select name="classroom" id="classroom" data-placeholder="Choose classroom" class="form-control chosen-select">
					<option value="0"></option>
					<?php
						foreach ($classrooms as $classroom) {
							echo "<option value='$classroom->id'>$classroom->name</option>";
						}
					?>
				</select>
				<input type="hidden" name="form" value="add-class">
				<input type="submit" name="submit" class="form-control" value="ADD CLASS"/>
			</form>
		</div>
	</div>
</div>
