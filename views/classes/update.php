<div class="container classes">
	<div class="row">
		<div class="col-md-1"></div>
		<div class="col-md-4">
			<div class="col-md-12">
				<div class="col-md-2"></div>
				<div class="col-md-8 center">
					<span class="fa fa-plus"></span>
					<a href="?controller=classes&action=add" class="add-classroom"></a>
				</div>
				<div class="col-md-2"></div>
			</div>
			<div class="col-md-12">
				<div class="col-md-2"></div>
				<div class="col-md-8 center">
					<span class="fa fa-edit"></span>
					<a href="?controller=classes&action=update" class="edit-classroom"></a>
				</div>
				<div class="col-md-2"></div>
			</div>
			<div class="col-md-12">
				<div class="col-md-2"></div>
				<div class="col-md-8 center">
					<span class="fa fa-trash"></span>
					<a href="?controller=classes&action=delete" class="delete-classroom"></a>
				</div>
				<div class="col-md-2"></div>
			</div>
			<div class="col-md-12 back">
				<div class="col-md-2"></div>
				<div class="col-md-8 center">
					<span class="fa fa-hand-o-left"></span>
					<a href="?controller=classes&action=show"></a>
				</div>
				<div class="col-md-2"></div>
			</div>
		</div>
		<div class="panel panel-classes col-md-4 center edit-class">
			<div class="panel-heading center">EDIT CLASS</div>
			<form method="post" action="?controller=classes&action=show">
				<select name="id" class="form-control" data-fields="select:professor_id,select:group_id,select:course_id,select:classroom_id" data-ajax="classes">
					<option value="0">Choose class...</option>
					<?php
						foreach ($classes as $class) {
						 	echo "<option value='$class->id'>Class #$class->id</option>";
						 }
					?>
				</select>
				<select name="professor_id" id="professor" class="form-control">
					<option value="0">Choose professor...</option>
					<?php
						foreach ($professors as $professor) {
							echo "<option value='$professor->id'>" . $professor->grade->name . " $professor->name</option>";
						}
					?>
				</select>
				<select name="group_id" id="group" class="form-control">
					<option value="0">Choose group...</option>
					<?php
						foreach ($groups as $group) {
							echo "<option value='$group[id]'>$group[major_name]-$group[serie_name]-$group[name]</option>";
						}
					?>
				</select>
				<select name="course_id" id="course" class="form-control">
					<option value="0">Choose course...</option>
					<?php
						foreach ($courses as $course) {
							echo "<option value='$course->id'>$course->name</option>";
						}
					?>
				</select>
				<select name="classroom_id" id="classroom" class="form-control">
					<option value="0">Choose classroom...</option>
					<?php
						foreach ($classrooms as $classroom) {
							echo "<option value='$classroom->id'>$classroom->name</option>";
						}
					?>
				</select>
				<input type="hidden" name="form" value="update-class">
				<input type="submit" name="submit" class="form-control" value="EDIT CLASS"/>
			</form>
		</div>
	</div>
</div>
