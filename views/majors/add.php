<div class="container majors">
	<div class="row">
		<div class="col-md-1"></div>
		<div class="col-md-4">
			<div class="col-md-12">
				<div class="col-md-2"></div>
				<div class="col-md-8 center">
					<span class="fa fa-plus"></span>
					<a href="?controller=majors&action=add" class="add-major"></a>
				</div>
				<div class="col-md-2"></div>
			</div>
			<div class="col-md-12">
				<div class="col-md-2"></div>
				<div class="col-md-8 center">
					<span class="fa fa-edit"></span>
					<a href="?controller=majors&action=update" class="edit-major"></a>
				</div>
				<div class="col-md-2"></div>
			</div>
			<div class="col-md-12">
				<div class="col-md-2"></div>
				<div class="col-md-8 center">
					<span class="fa fa-trash"></span>
					<a href="?controller=majors&action=delete" class="delete-major"></a>
				</div>
				<div class="col-md-2"></div>
			</div>
			<div class="col-md-12 back">
				<div class="col-md-2"></div>
				<div class="col-md-8 center">
					<span class="fa fa-hand-o-left"></span>
					<a href="?controller=majors&action=show"></a>
				</div>
				<div class="col-md-2"></div>
			</div>
		</div>
		<div class="panel panel-info col-md-4 center add-major">
			<div class="panel-heading center">NEW MAJOR</div>
			<form method="post" action="?controller=majors&action=show">
				<input type="text" name="name" class="form-control" placeholder="Name">
				<input type="hidden" name="form" value="add-major">
				<input type="submit" name="submit" class="form-control" value="ADD MAJOR"/>
			</form>
		</div>
	</div>
</div>
