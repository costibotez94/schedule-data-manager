<div class="container majors">
	<div class="row">
		<div class="panel panel-info col-md-7 majors-table">
			<div class="panel-heading center">MAJORS TABLE</div>
		  	<table class="table table-hover">
				<thead>
					<tr>
					<?php
						$thead = get_class_vars('Major');
						foreach ($thead as $th => $v) {
							echo '<th>' . strtoupper($th) . '</th>';
						}
					?>
					</tr>
				</thead>
				<tbody>
					<?php
						foreach ($majors as $major) {
							echo '<tr>';
							echo 	'<td>' . $major->id 	. '</td>';
							echo 	'<td>' . $major->name 	. '</td>';
							echo '</tr>';
						}
					?>
				</tbody>
			</table>
		</div>
		<div class="col-md-1"></div>
		<div class="col-md-4">
			<div class="col-md-12">
				<div class="col-md-2"></div>
				<div class="col-md-8 center">
					<span class="fa fa-plus"></span>
					<a href="?controller=majors&action=add" class="add-faculty"></a>
				</div>
				<div class="col-md-2"></div>
			</div>
			<div class="col-md-12">
				<div class="col-md-2"></div>
				<div class="col-md-8 center">
					<span class="fa fa-edit"></span>
					<a href="?controller=majors&action=update" class="edit-faculty"></a>
				</div>
				<div class="col-md-2"></div>
			</div>
			<div class="col-md-12">
				<div class="col-md-2"></div>
				<div class="col-md-8 center">
					<span class="fa fa-trash"></span>
					<a href="?controller=majors&action=delete" class="delete-faculty"></a>
				</div>
				<div class="col-md-2"></div>
			</div>
			<div class="col-md-12 back">
				<div class="col-md-2"></div>
				<div class="col-md-8 center">
					<span class="fa fa-hand-o-left"></span>
					<a href="?controller=pages&action=home"></a>
				</div>
				<div class="col-md-2"></div>
			</div>
		</div>
	</div>
</div>
