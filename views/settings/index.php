<div class="container settings">
	<div class="row">
		<div class="panel panel-default col-md-7 setting-table">
			<div class="panel-heading center">SETTINGS</div>
		  	<form method="post" action="?controller=settings&action=show">
				<?php
					foreach ($settings as $setting) {
						if(in_array($setting->setting_key, array('major', 'language'))) {
							echo '<input name="setting_key" type="hidden" value="'. $setting->setting_key .'"/>';
							echo '<select name="setting_value" class="form-control">';
								if($setting->setting_key == 'major') {
									echo '<option value="0">Choose default major...</option>';
									foreach ($majors as $major) {
											echo '<option value="' . $major->id . '"' . ($major->id == $major_id ? "selected" : "") . '>' . $major->name . '</option>';
									}
								}
								if($setting->setting_key == 'language') {
									echo '<option value="0">Choose default language...</option>';
									foreach ($languages as $id => $language) {
											echo '<option value="' . $id . '"' . ($id == $language_id ? "selected" : "") . '>' . $language . '</option>';
									}
								}
							echo '</select>';
						} else {
							echo '<input type="text" name="' . $setting->setting_key . '" value="' . $setting->setting_value . '"/>';
						}
					}
				?>
				<input type="hidden" name="form" value="update-setting">
				<input type="submit" name="submit" class="form-control" value="SAVE SETTINGS">
			</form>
		</div>
		<div class="col-md-1"></div>
		<div class="col-md-4">
			<div class="col-md-12 back">
				<div class="col-md-2"></div>
				<div class="col-md-8 center">
					<span class="fa fa-hand-o-left"></span>
					<a href="?controller=pages&action=home"></a>
				</div>
				<div class="col-md-2"></div>
			</div>
		</div>
	</div>
</div>
